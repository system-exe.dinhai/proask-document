# ProAsk

## TimeLine
```mermaid
    gantt
        title ProAsk Roadmap
        dateFormat  YYYY-MM-DD
        excludes weekends
        
        section REDMINE CLONE
            Desing Application :r-clone, 2023-03-01, 14d
            Design UI :r-clone2, 2023-03-08, 14d
            Coding Api : r-clone3, 2023-03-07, 14d
            Coding UI : r-clone4, 2023-03-15, 14d
            Testing And FixBug  : 2023-03-25, 14d
```
## Role
|Name|Role|
|-|-|
|Đình Ái|Design App, Design UI, API, UI, Testing|
|Thanh Tân|Design App, Design UI, UI, Testing|
|Thùy Kim|Design App, API, Testing|
|Hồng Hoa|Design App, Design UI, Testing|
|Phú Hồng|Design App, Design UI, Testing|
|Ngọc Thiện|Design App, API, UI, Testing|
|Huỳnh Đức|Design UI, UI|

## Wiki
- [Database Viewer](https://dbdiagram.io/d/63c4fbe3296d97641d79dca4)
- [Figma File](https://www.figma.com/file/W0DzU9OPplBXJm2IFXsOUQ/ProAsk)
## Figma Viewer

[Figma Viewer](https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FW0DzU9OPplBXJm2IFXsOUQ%2FProAsk%3Fnode-id%3D5%253A1635%26t%3DhQ0BWDMqptgau1c8-1 ':include :type=iframe width=100% height=700px')