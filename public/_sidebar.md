
* [Home](/)
  
- Authentication
  - [Register](vi/authentication/register)
  - [Login](vi/authentication/login)
  - [Forget Password](vi/authentication/forgot_password)

- Admin 
  - User Management
    - [List User](vi/admin/user-management/list_user)
    - [Add New User](vi/admin/user-management/add_new_user)
    - [Change Info User](vi/admin/user-management/change_info_user)
    - [SubDashboard User (Detail user)](vi/admin/user-management/sub_dashboard_user)
- User
  - MyPage (For user in Session)
    - [Change Info User](vi/user/mypage/change_info_user)
  - Project
    - [List Project](vi/user/project/list_project)
    - [Add New Project](vi/user/project/add_new_project)
    - [Edit info](vi/user/project/edit_info)
    - [Detail](vi/user/project/overview)
    - [Edit status](vi/user/project/edit_status_of_project)
    - [Edit wiki](vi/user/project/edit_wiki_of_project)
    - [Wiki (TanHong)](vi/user/project/wiki)
    - [List Invited Member (HongTan)](vi/user/project/list_invited_member)
- Task
    - [List Task](vi/user/task/list_task)
    - [Add New Task](vi/user/task/add_new_task)
    - [Detail Task](vi/user/task/detail_task)
    - [Edit Task](vi/user/task/edit_task)
    - [Task Comment and File Upload](vi/user/task/task_comment_and_file_upload)
- Group Tasks
    - [List Group Task](vi/user/groupTask/list_groupTask)
    - [Grop Task Detail ](vi/user/groupTask/list_groupTask_Detail)
