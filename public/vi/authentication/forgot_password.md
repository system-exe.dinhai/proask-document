# Authentication - Forgot Password
## Mô tả
* Cho phép người dùng reset password(quên mật khẩu)

Thông tin hiển thị màng hình:

| Input fill  | Input type	 | Enable/Disable | Required |DB | Validate |
|------ |------ |------ |------ |------ |------ |
| Email cần reset password | text | Enable |X|user.email|phải có tồn tại|

|Tên Button  |Điều Kiện Hiện  |
|--|--|
|  Đăng kí| Luôn hiện |
|  Quay Lại| Luôn hiện |

### Flow Diagram

```mermaid
flowchart LR
    A[LoginPage] -->|Click ResetPassword| C[Popup ResetPassword]
    C --> A
```

### Logic:

* User nhập `email` vào input rồi nhấn submit.
* Kiểm tra `email` có tồn tại hay không:
    ```sql
    SELECT * 
    FROM user Join user_profile on user.id = user_profile.user_id
    WHERE email = 'example@example.com';
    ```
    * Nếu không có. Trả về code `400` và `data`.
    * Nếu có. 
        * Trả về code `200` và `data`. 
        * Redirect về trang `Login`.
        * Hiện thông báo thành công
            
            Bạn đã khôi phục mật khẩu thành công, hãy vào `email` để khôi phục lại mật khẩu.

        * và gửi Mail cho `user.email` theo template:

    ```
    Chào `user.name`,
            
    Bạn đã khôi phục mật khẩu thành công.

    Vui lòng vào dường dẫn [../user/change-password/#user.id](https://link-url-here.org) để lấy lại mật khẩu mới.

    Lưu ý: Đường dẫn sẽ hết hạn sau 24h.
    ```

### User Change password: