


# ProAss: Page Login
## Mô tả
* Cho phép người dùng nhập Email và Password của mình để Login vào ProAsk.
* Cho phép người dùng reset password(quên mật khẩu)

Thông tin hiển thị màng hình:

| Input fill  | Input type	 | Enable/Disable | Required |DB | Validate |
|------ |------ |------ |------ |------ |------ |
| Email đăng nhập | text | Enable |O|user.email|phải có tồn tại|
| Mật khẩu | text | Enable |O|user.password|check request.password == user.password|

|Tên Button  |Điều Kiện Hiện  |
|--|--|
|  Đăng kí| Luôn hiện |
|  Quay Lại| Luôn hiện |
### Flow Diagram

```mermaid
flowchart LR
    A[LoginPage] -->|Submit| B[DashBoard]
```
### Logic:

* User nhập `email` và password vào input rồi nhấn submit
* Validate:
    - `email`: có tồn tại hay không.
    - `password`: có trùng với `user.password` hay không.

Nếu bị dín validate. Trả về response HTTP code là `400` và `data`.


* Select `user` ra, rồi kiểm tra `email` và `password` có trùng với `user.email` và `user.password` trong DB hay không:

    ```sql
    SELECT * FROM User WHERE email = 'example@example.com';
    ```

    * Nếu không đúng. Trả về response HTTP code là `400` và `data`
    * Nếu đúng. Trả về `200` và `data`(thông tin của user)

* Sau khi Login:
    Lưu thông tin người dùng vào session hoặc tạo JWT. 
    Redirect đến trang chính hoặc trang đã yêu cầu trước đó















