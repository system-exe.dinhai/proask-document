
# Authentication - Register

## Mô tả
 
- Người dùng thực hiện đăng ký với email và thực hiện verify email
- Sau khi verify email sẽ được chuyển tới đường link đăng ký các thông tin cá nhân
- Sau đó tài khoản sẽ vào danh sách chờ approve từ phía admin

## Flow Diagram

```mermaid
graph LR

L[Đăng Ký Email] -- 'Đăng ký và Nhận email verify' --> A[Đăng ký thông tin cá nhân ]
A[ Verify Email ]  --'Xác nhận đăng ký thông tin cá nhân'-->P[Lưu thông tin cá nhân]
```

## Logic
- Màn hình đăng ký
  
  1. Người dùng nhập thông tin email và click Đăng Ký
      - Input `email` không được bỏ trống và không được trùng trong table `users` hoặc table `temp_user_registers` trong redis
      - Thông tin `email`, `token` (Mã Hash tự sinh), `is_verify` (default `false`) sẽ được lưu vào table `temp_user_registers` trong database Redis.
      - Gửi email yêu cầu verify đến cho `email` đã đăng ký với nội dung "Yêu cầu click đường dẫn để xác nhận email". Đường dẫn sẽ là `/email-verify/{email}/{token}`
       - Trả về response HTTP code là `200` và `data`
            - Example result:
              ```json
              {
                code: 200,
                message: "Register user email successfully, please verify email"
                data: {
                  email: <email_user> 
                }
              }
              ```
  2. Người dùng vào đường dẫn verify ``/email-verify/{email}/{token}`
      - Hệ thống tiến hành kiểm tra email và token có tồn tại trong table `temp_user_registers` trong database Redis.
        - Nếu có trả về data success
          - Cập nhật `is_verify` về `true`
          - Trả về response HTTP code là `200` và `data`
            - Example result:
              ```json
              {
                code: 200,
                message: "Email verify successfully"
                data: {
                  email: <email_user> 
                }
              }
              ```
        - Nếu kiểm tra đúng Email và Token đúng nhưng `is_verify` là `true`
          - Trả về HTTP code `400` và `data`
            - Example result:
              ```json
              {
                code: 400,
                message: "Email is verified"
                data: {
                  email: <email_user> 
                }
              }
              ```
        - Nếu kiểm tra không đúng Email hoặc Token
          - Trả về HTTP code `400` và `data`
            - Example result:
              ```json
              {
                code: 400,
                message: "Not Found"
                data: {}
              }
              ```
  3. Người dùng tiến hành đăng ký thông tin cá nhân
    
    - Sơ lược Input

    |**Input fill** |**Input type**   | **Enable/Disable** | **Required**|**DB** |**Validate**|
    |---|---|---|---|---|---|
    |Tên nhân viên | txt |Enable| O | userProfile.full_name| Text (255) / không chứa kí tự đặc biệt, số|
    |Thông tin sơ lược | txt |Enable| X | userProfile.resume| Text (552)|
    |Chức vụ  | Drop box |Enable| O |  userProfile.role|  |
    |Số điện thoại  | txt |Enable| X |  userProfile.phone| Int (10) |
    |Ngày sinh  | txt |Enable| X|  userProfile.birthday| date|
    |Địa chỉ | txt | Enable|X |  userProfile.addess| Text (255)|
    |Ảnh đại diện | txt |Enable| X |  userProfile.has_avatar| href|

    - Xử lý lưu thông tin người dùng
      - Kiểm tra trường image_avatar có tồn tại hay không:
        - Nếu có tiến hành cập nhật vào Table `global_attachments` và đặt biến cho `has_avatar` là `true`
        - Tiến hành cập nhật thông tin vào table `user`
          - column `is_approved` mặc định sẽ luôn là `false`
          ```sql
            INSERT INTO users (email, password, is_approved, created_at, updated_at)
            VALUES ('nguyenvana@proask.com', 'password123', false, '2022-12-01 10:00:00', '2023-03-08 09:30:00');
            ```
        - Tiến hành cập nhật thông tin vào table `user_profiles`
        ```sql
          INSERT INTO user_profiles (full_name, resume, role, phone, birthday, address, has_avatar, created_at, updated_at)
          VALUES ('Nguyễn Văn A', 'Lorem ipsum dolor sit amet', 'Software Engineer', '0987654321', '1995-01-01', 'Hanoi, Vietnam', true, '2022-12-01 10:00:00', '2023-03-08 09:30:00');
        ```
        - Tiến hành xóa email trong `temp_user_registers` trong database redis
    - Khi thành công trả về HTTP code `200` và data
      - Example result:
          ```json
          {
            code: 200,
            message: "Create user successfully"
            data: {
              full_name: 'Nguyễn Văn A'
              email: 'nguyenvana@proask.com' 
              resume: 'Lorem ipsum dolor sit amet'
              role: 'Software Engineer'
              phone: '0987654321'
              birthday: '1995-01-01'
              address: 'Hanoi, Vietnam'
              avatar: 'https://proask.com/avatar/user.png'
              created_at: '2022-12-01 10:00:00', 
              updated_at: '2023-03-08 09:30:00'
            }
          }
          ```
      
