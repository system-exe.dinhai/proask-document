# User - Group Task - List Group Task


- Danh sách hiển các Group task đã tạo

### Di chuyển màn hình

```mermaid
graph LR
A[Dashboard] -- click Menu --> B[List Group task]--Create-->H{Create Group task}
```
### Search group task
 Mô tả:
-   Thực hiện search danh sách group task theo những mục tiêu mong muốn nhất định
-   Chỉ hiển thị những group task mà user đã tạo 

Logic:
- Search theo key input

|Item  |Type  |Search  |DB   | Note|
|--|--|--|--|--|
|Group task name |Text  | Like  | taskGroups.name|
|User name |Text  | Like  | taskAssingeUser.userID|
|Task name |Text  | Like  | tasks.name|
|Task ID |Text  | Equal  | tasks.name|

-   Hiển thị button:
    -   Search
        -   Thực hiện search
    -   Clear
        -   Xóa những giá trị đã được nhập


### Hiển thị Group task

Mô tả: Hiển thị danh sách group task theo dạng grid
Logic: 
-   Hiển thị danh sách Group task giảm dần theo end time của group đó
-   Nếu group task đã được delete: Không hiển thị (project có  `'taskGroups.[deleted_at] != NULL'`)
-   STATUS của PROJECT: thành từng label nhỏ cạnh PROJECT NAME theo màu sắc vd:
    -   Waiting: màu vàng
    -   in_progress: màu xanh nhợt
    -   Completed: màu xám
-   Hiển thị tổng số lượng record
-   Hiển thị button:
    - Create New Group Task
    - Edit Group Task
 
Kết quả mong muốn: Hiển thị dạng grid giống project chứa các thông tin sau

| Group Task ID |Name  |% Hoàn Thành|Số lượng task done/Tổng số lượng task |Start time|End time| Button|
|--|--|--|--|--|--|--|
| 123 |  Task A|[Biểu đồ tròn]|8/11|2021/01/01|2021/01/02|[Icon pencil](http://)|
| 231 |  Task B|[Biểu đồ tròn]|4/12|2021/01/01|2021/01/02|[Icon pencil](http://)|
|  `taskGroups.id` |  `taskGroups.name`|`% = Số lượng task done/Tổng số lượng task *100  `|`task.status != Close`|`taskGroups.start_time`|`taskGroups.end_time`||

-   Biểu đồ: 
    - Hiện màu xanh khi toàn bộ các task chưa có task nào bị cháy (now > task.end_time + ngày warming)
    - Hiện màu vàng khi có 1 task dang warming (task.end_time > now >= task.end_time + ngày warming)
    - Hiện màu đỏ khi có 1 task đang cháy (now < task.end_time )

![MarineGEO circle logo](/assets/img/GroupTaskProgress.jpg  "MarineGEO logo")


