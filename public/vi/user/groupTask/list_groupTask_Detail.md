# User - Group Task - List Group Task


- Danh sách hiển các Group task đã tạo

### Di chuyển màn hình

```mermaid
graph LR
B[List Group task]--Create-->H{Create Group task}--Save-->k[Group task Detail]
B--Edit-->k
k--Back-->B
k--Add new-->H
```
### Create group task Dialog
 Mô tả:
-   Hiện dialog cho phép nhập tên group task add các task 

Logic:
- Nhập tên cho group task
- Có fill input cho user select task
- Task được chọn sẽ hiển thị như table dưới

|Task ID  |Task Name  |Assignee  |Button   |
|--|--|--|--|
|1445 |Pro task 1  | Kirito  | Remove Icon|
|1234 |Pro task 2  | Bell  | Remove Icon|


-   Hiển thị button:
    -   Save
        -   Thực hiện Save data xuống DB
        `
        SQL
    `
    -   close
        -   Đóng dialog


### Group task Detail

Mô tả: Hiển thị thông tin chi tiết group task như list dưới
Header:


| Group Name |Button  |Button
|--|--|--
| Task A |Edit/Save ICON  |Add ICON

Add:
Show Create group task Dialog

Body:

- Hiển bảng chứa Thông tin sau
    |Tổng Task|Task đã Done|Task Còn lại | Task cháy|Gruop task end time|
    |--|--|--|--|--|
    |8|2|6|1|2022/01/01|

- Hiển thị Biểu đồ cột hiển thị các giá trị sau
    - Trục Tung: Số lượng task
    - Trục Hoành: Tên Người đang assigne
    - Số task đảm nhận
    - Số task cháy
    - Số task done

- Hiển thị lits chi tiết dưới đây
    -   Hiển thị danh sách task giảm dần theo end time của task

|Task ID|Task name  |Task type|Status  |Assignee|Start Time |End Time|Button
|--|--|--|--|--|--|--|--
|Task ID|Task 1 |Bug |In progress  |Bell|01/01/2020|01/02/2020|Remove ICON
|Task ID|Task 2  |Feature|In progress  |Bell|01/01/2020|01/02/2020|Remove ICON
|`task.id`|`task.name`  |`task.task_type_id`|`task.status_id`|`task.task_assigne_user`  |`task.start_time` | `task.end_time`|

Remove:
Hiện dialog xác nhận
- yes: update deleted_at
	`SQL`
-No: đóng dialog




