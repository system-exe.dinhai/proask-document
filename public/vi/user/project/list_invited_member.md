# User - Project - Invite Member




## List Invited Member
- Chức năng cho phép **Thêm mới** cũng như **Remove** member ra khỏi dự án.
- Cho phép thiết lập **Role** của từng member trong dự án

### Di chuyển Màn hình

```mermaid
graph LR
L[Member List] -- 'Thêm mới' --> A[Dialog: Invite menber] --'Add'--> L
```


### Button

|Tên Button  |Điều Kiện Hiện  |
|--|--|
|  Invite| Luôn hiện |
|  Delete| Luôn hiện |

### Dropbox

|Tên Dropbox  |Điều Kiện Hiện  |
|--|--|
|  Edit Role| Luôn hiện với mỗi Member |

### Search member


| Item |Type  |Search|DB|Note|
|--|--|--|--|--|
| Menber's name | Input  | Like |user_profile.name ||
| Menber's Email | Input  | Like |user.name ||
| Menber's ID | Input  | equal |user_profile.user_id ||

-Không search được các member có **is_active = False**



### Invite list


|No|**Name** |**Role**   |add date| **Button** | 
|----|------------|---|---|--|
|1  | Name User A [Icon Role] |Dev (Drop box)|01/01/2022|[Icon Edit/Save]
|2  | Name User B [Icon Role] |QC (Drop box)|01/01/2022|[Icon Edit/Save]
|3  | Name User B [Icon Role] |MG (Drop box)|01/01/2022|[Icon Edit/Save]
|4  | Name User D [Icon Role] |Dev Lead (Drop box)|01/01/2022|[Icon Edit/Save]
|-- | ```user_profiles.name where user_profiles.ID = project_member.user_id```|```project_member.role```|```project_member.added_at```|

- Không hiển thị mểm có **is_active = flase**



### Invite 

- Click [Invite] --> Dialog [Add menber]
- Input data vào mục seach sau đó hiển thị drop list theo điều kiện seach
	- **member_id và project_id**  tồn tại trong bảng Project_member  và **is_active = true** thì không hiện lên drop list 

	```SQL
	SELECT * 
	FROM user 
	WHERE [
		project_members.userId = user_id,
		project_members.is_active = true,
	]
	```

- Click chọn user trên drop list sẽ dc add vào list trên dialog

	- **member_id và project_id** chưa tồn tại trong bảng Project_member thì cho add
	- **member_id và project_id**  tồn tại trong bảng Project_member  và **is_active = false** thì update lại cờ **is_active = true** và  **Project_member.role**
	
	|Data |project_member | 
	|----|------------|
	|userProfile | user_id |
	|Project.id  | project_id|
	|Data in put  | role |
	| true | is_active| |

	```SQL
	INSERT INTO ProjectUser (user_id, project_id, role)
	VALUES [
		(:user_id, :project_id, :role),
		(:user_id, :project_id, :role),
		(:user_id, :project_id, :role),
		(:user_id, :project_id, :role),...
	]
	```

### Edit

- Click vào **Dropbox** của Member muốn Edit
- Chọn **Role**

	```SQL
	UPDATE ProjectUser SET role = 'new_role' WHERE id = 'user_id';
	```


### Xóa
- Click vào btn **Delete** của Member muốn Edit
- Hiện popup **Bạn có thật sự muốn xóa 'user_name' ra khỏi Project**:
	- Click **Có**, update **deleted_at** vào ProjectUser.deleted_at

	```SQL
	UPDATE ProjectUser SET deleted_at = Now() WHERE id = 'user_id';
	```

	- Click **Không**, tắt popup quay lại màng hình **List Invited Member**





	

