
# User - Project - Overview of Project

- **Quyền truy cập:** User thuộc dự án.
- Xem tổng quan danh sách task.
- Xem tổng quan timeline.
- Xem tổng quan user của dự án.

## Di chuyển màn hình

```mermaid
graph LR
A[Menu top] --Click vào menu--> B[Quản lý Project]
B --Click vào project--> C[Overview]
C --Quay lại--> B
C --Click vào user--> D[Detail User]
A[Menu top] --Click project--> C[Overview]
C --Click xem--> E[Danh sách Status của dự án]
C --Click xem--> F[Danh sách Wiki của dự án]
C --Click xem--> G[Danh sách User của dự án]
C --Click xem--> H[Danh sách Timeline của dự án]
C --Click xem--> I[Danh sách Task của dự án]
```

## Logic
- ### Tổng quan về task
    - Câu sql
        ```sql
        SELECT task_type.name, count(*) as total_of_task,
            SUM( CASE WHEN task.is_close != 1 THEN 1 ELSE 0 END ) as total_of_task_open,
            SUM( CASE WHEN task.is_close = 1 THEN 1 ELSE 0 END ) as total_of_task_status_closed
        FROM task_type
        LEFT JOIN task
        ON  task_type.id  =  task.task_type_id
        where task.project_id = [param project id]
        GROUP BY task_type.name
        ```
    - Hiển thị danh sách
        |  |open |closed|Total|
        |------|-----|-----|-----|
        | [Bug]() |[4]()|[16]()|[20]()|
        | [Feature]() |[3]()|[20]()|[23]()|
        | [Support]() |[4]()|[5]()|[9]()|
        | [Task]() |[10]()|[12]()|[22]()|
        | `task_type.name`|`total_of_task_open`|`total_of_task_status_closed`|`total_of_task`|
        - `task_type.name`: click vào đến màn hình danh sách task của project với type đã chọn
        - `total_of_task_open`: Những task có trạng thái khác closed. Click vào đến màn hình danh sách task với type đã chọn và stask.is_close != 1
        - `total_of_task_status_closed`: Những task có trạng thái closed. Click vào đến màn hình danh sách task với type đã chọn và stask.is_close = 1
        - `total_of_task`: Tổng task. click vào đến màn hình danh sách task với type đã chọn
    - Trạng thái task chưa closed

    ```mermaid
        pie
            title Task open
            "Bug" : 4
            "Feature" : 3
            "Support" : 4
            "Task" : 10
        ```

    - Trạng thái tất cả task

    ![MarineGEO circle logo](/assets/img/staus_of_task.png "MarineGEO logo")
  
- ### Tổng quan users
    - Danh sách User tham gia dự án
        - Câu sql
            ```sql
            SELECT project_user.role, user_profile.name
            FROM project_user
            LEFT JOIN user_profile
            ON  project_user.user_id  =  user_profile.id
            where project_user.project_id = [param project id]
            GROUP BY project_user.role, user_profile.name
            ```
        - Hiển thị danh sách
            |Vai trò  |Users |
            |------|-----|
            | project_manager |[Thạch Anh Tuấn](http://link_deatil_user)|
            | developer |[Kim Nguyễn](http://link_deatil_user), [Ngọc Thiện](http://link_deatil_user), [Tân Bùi](http://link_deatil_user), [Đức Lê](http://link_deatil_user)|
            | tester |[Hồng Hoa](http://link_deatil_user), [Phú Hồng](http://link_deatil_user)|
            | project_leader |[Đình Ái](http://link_deatil_user)|
            | `project_user.role`|`user.name`|

    - User task: Hiển thị biểu đồ các user với các task có trạng thái open
        - Câu sql
            ```sql
            SELECT project_user.name
            FROM project_user
            LEFT JOIN user_profile
                ON  user_profile.user_id  =  project_user.user_id
            LEFT JOIN task_assign_user
                ON  task_assign_user.user_id  =  project_user.id
            LEFT JOIN task
                ON  task_assign_user.task_id  =  task.id
            where project_user.project_id = [param project id]
                and task.is_draft = false
                and task.is_close = false
                and task.is_new = true
                and task.deleted_at = null
            GROUP BY user_profile.name
            ```
        - Biểu đồ

        ![MarineGEO circle logo](/assets/img/total_task.png "MarineGEO logo")

- ### Log task chuyển trạng thái
    - Lấy dữ liệu comment theo task của 3 ngày gần nhất
    - Hiển thị ngày, tên task, loại task, trạng thái, người tạo, các comment trong 3 ngày của task (theo thứ tự thời gian tăng dần)
    
    - Câu sql
        ```sql
        SELECT user_profile.name, task.name, project_task_status.name,project_task_type.name, task.created_at, task.id
        FROM task
        LEFT JOIN task_assign_user
            ON  task_assign_user.task_id  =  task.id
        LEFT JOIN user_profile
            ON  user_profile.user_id  =  task.created_by
        LEFT JOIN project_task_status
            ON  project_task_status.id  =  task.project_task_status_id
        LEFT JOIN project_task_type
            ON  project_task_type.id  =  task.task_type_id
        LEFT JOIN task_comment
            ON  task_comment.task_id  =  task.id
        where task.project_id = [param project id]
            and task.is_draft = false
            and task.deleted_at = null
        order by task_comment.created_at
        GROUP BY user_profile.name, task.name, project_task_status.name,project_task_type.name, task.created_at
        ```

    - Ví dụ
        
        - **2023-03-20** 
            - **10:20** (New) Feature #100: Màn hình Login 
                - Tạo mới màn hình login
                - Connect API`
                    
                    - **11:30** (InProcess) [Kim Nguyễn](http://link_deatil_user)
                        - Tạo nhánh feature_100
                        - ....
                        
                    - **15:40** (Resoled) [Kim Nguyễn](http://link_deatil_user)
                        - Đã unit test
                        - ....
                    - **16:50** (Passed) [Hoa Lê](http://link_deatil_user)
                        - Đã test ok
                        - ....
            - **13:50** (New) Feature #100: Màn hình quản lý user
                - Tạo mới màn hình user
                - Tiềm kiếm

        - **2023-03-19** 
            - **10:20** (New) Feature #100: Màn hình Login 
                - Tạo mới màn hình login
                - Connect API`
                    
                    - **11:30** (InProcess) [Kim Nguyễn](http://link_deatil_user)
                        - Tạo nhánh feature_100
                        - ....
                        
                    - **15:40** (Resoled) [Kim Nguyễn](http://link_deatil_user)
                        - Đã unit test
                        - ....
                    - **16:50** (Passed) [Hoa Lê](http://link_deatil_user)
                        - Đã test ok
                        - ....
            - **13:50** (New) Feature #100: Màn hình quản lý user
                - Tạo mới màn hình user
                - Tiềm kiếm
            
        
 