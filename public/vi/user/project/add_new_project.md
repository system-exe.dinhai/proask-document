
# User - Project - Add New Project

- **Quyền truy cập:** Mọi user đều có quyền
- Thông tin cơ bản của dự án.
- Tạo danh sách status cho task của dự án.
- Thêm user tham gia vào dự án.

## Di chuyển màn hình

```mermaid
graph LR
A[Menu top] --Click vào menu--> B[Quản lý Project]
B --Click vào Tạo mới--> C[Tạo mới Project]
C --Quay lại--> B
C --Click vào Tạo mới user--> D[Tạo mới User]
A[Menu top] --Click tạo mới project--> C[Tạo mới Project]
```

## Logic
- ### Màn hình tạo mới project bao gồm 3 tab:
    - #### Tab1: Thông tin project
        - Thêm mới project bao gồm các thông tin

        |**Input fill** |**Input type**   | **Enable/Disable** | **Required**|**DB** |**Validate**|
        |----|------------|---|---|-------|------------|
        |Tên | Text box |Enable| O |  project.name| Text (225) / Unique |
        |Ngày bắt đầu | Date |Enable| O |  project.start_date| date (Y-m-d) |
        |Ngày kết thúc | Date |Enable| O |  project.end_date| date (Y-m-d), lớn hơn Ngày bắt đầu |
        |Mô tả | CKEditor |Enable| O |  project.description| Text |
        | |  |  | |  project.status = not_started| |
        | |  |  | |  project.created_by = user login| |

    - #### Tab2: Status of task
        - Hiển thị danh sách trạng thái task.

        | STT |Tên |Vị trí||
        |------|-----|-----|-----|
        | 1 |Draft|1||
        | 2 |New|2||
        | 3 |Đang làm|2|[btn_delete]|
        | 4 |Đợi test|3|[btn_delete]|
        ...
        | 5 |Closed|end||
        | |`project_task_status.name`|`project_task_status.position`|`[btn_delete]`|
            - `project_task_status.name`: Textbox hiển thị giá trị
            - `project_task_status.position`: Textbox hiển thị giá trị.(Hiện tại cho nhập giá trị version sau không cho nhập nữa mà kéo thả chọn vị trí)
            - `[btn_delete]`: Xóa Status đã chọn ra khỏi danh sách. Không xóa được các giá trị mặc định.
            - Các giá trị status là: 
                - Mặc định: Draft, New ở đầu tiên. Hiển thị lable không cho chỉnh sửa
                - `Các giá trị được thêm mới sẽ nằm ở khoảng giữa`.
                - Mặc định: Giá trị cuối cùng là Closed. Hiển thị lable không cho chỉnh sửa

        - Nút thêm mới: Add thêm 1 row vào danh sách trạng thái. (add vào vùng ở giữa các giá trị mặc định)
        - Validate mỗi row của danh sách:

        |**Input fill** |**Input type**   | **Enable/Disable** | **Required**|**DB** |**Validate**|
        |----|------------|---|---|-------|------------|
        |Tên | Textbox |Enable| O |  project_task_status.name| Text (225) / Unique |
        |Vị trí| Textbox |Enable| O |  project_task_status.position| number|

        - Lưu ý: có thể thêm hoặc không (không bắt buộc thêm Status).

    - #### Tab3: User
        - Hiển thị danh sách User được chọn tham gia vào dự án.

            | STT |Tên user |Vai trò hiện tại|Vai trò dự án||
            |------|-----|-----|-----|-----|
            | 1 |[Kim Nguyễn](http://link_deatil_user) |developer|Select chọn role|[btn_delete]|
            | 2 |[Hoa Lê](http://link_deatil_user) |tester|Select chọn role|[btn_delete]|
            | |`user_profile.name`|`user_profile.role`|`Select chọn role`|`[btn_delete]`|

            - `Select chọn role`: ['project_manager' => 'Manager', 'developer' => 'Developer', 'tester' => 'Tester', 'project_leader' => 'Leader']
            - `[btn_delete]`: Xóa user đã chọn ra khỏi danh sách

        - Nút chọn user: Hiển thị modal load danh sách các user hiện có
            - Câu sql:
            ```sql
            SELECT user_profile.name, user_profile.id, user_profile.role
            FROM user_profile
            ```

            - Hiển thị danh sách: nếu user đã được chọn trước đó thì hiện checkox, mặc định là không check

                | STT |Tên user |Vai trò hiện tại|Checkbox All|
                |------|-----|-----|-----|
                | 1 |[Kim Nguyễn](http://link_deatil_user) |developer|CheckBox|
                | 2 |[Hoa Lê](http://link_deatil_user) |tester|CheckBox|
                | |`user_profile.name`|`user_profile.role`|`checkbox chọn hoặc không`||
                - `Checkbox All`: checkbox chọn hoặc hủy chọn tất cả

            - Textbox nhập tên và button tìm kiếm theo tên. Nhấp button thực hiện tìm kiếm.
                ```sql
                SELECT user_profile.name, user_profile.id, user_profile.role
                FROM user_profile
                where user_profile.name like %[param name search]% or where user_profile.email = %[param name search]%
                ```

            - Button cancel: nhấp hủy bỏ thao tác chọn => đóng modal => ngưng xử lý.
            - Button accept: 
                - Lấy thông tin user đã chọn hiển thị lên danh sách user bên dưới.
                - Đóng modal.
        - Nút Thêm mới user: Nếu muốn thêm user chưa tồn tại trong hệ thống => link đến màn hình thêm mới user.
        - Lưu ý: có thể thêm hoặc không (không bắt buộc thêm Wiki).

- ### Nút lưu thông tin: sau khi kiểm tra pass validate -> xử lý lưu DB
    - Bước 1: Lưu Thông tin project tương ứng với thông tin nhập ở **#Tab1**

    ```sql
    INSERT INTO project (name, description, start_date, end_date, status, created_by, created_at, updated_at)
    VALUES ([input name], [input description], [input start_date], [input end_date], 'not_started', <user_login>, <datetime now>, <datetime now>);
    ```

    - Bước 2: Lưu Thông tin ProjectTaskStatus tương ứng với thông tin nhập ở **#Tab2**: chỉ lưu các giá trị được thêm => không thêm giá trị mặc định
        - Nếu danh sách status rỗng => ko xử lý
        - Nếu có danh sách status:
        ```sql
        INSERT INTO project_task_status (name, position, project_id, created_at, updated_at)
        VALUES ([input name], [input position], <project_id ở bước 1>, <datetime now>, <datetime now>),
        VALUES ([input name], [input position], <project_id ở bước 1>, <datetime now>, <datetime now>)...;
        ```

    - Bước 3: Lưu Thông tin ProjectUser tương ứng với thông tin nhập ở **#Tab3**
        - Thêm thông tin mặc định của người tạo dự án:
            ```sql
            INSERT INTO project_user (role, user_id, project_id, created_at, updated_at)
            VALUES ('project_manager', <user login> , <project_id ở bước 1>, <datetime now>, <datetime now>)
            ```
        - Thêm thông tin danh sách user được add vào:
            - Nếu danh sách user rỗng => ko xử lý
            - Nếu có danh sách user:
                - Câu sql insert
                ```sql
                INSERT INTO project_user (role, user_id, project_id, created_at, updated_at)
                VALUES ([input role], [input user_id], <project_id ở bước 1>, <datetime now>, <datetime now>),
                VALUES ([input role], [input user_id], <project_id ở bước 1>, <datetime now>, <datetime now>)...;
                ```
                - Gửi mail cho các user được add vào dự án:
                    - Câu sql select thông tin user (#sql3.1)
                    ```sql
                    SELECT user_profile.name, user.id, user.email
                    FROM user_profile
                    LEFT JOIN User
                    ON  user.id  =  user_profile.user_id
                    where user_profile.user_id IN [danh sách user_id được chọn thêm vào dự án]
                    ```
                    - Câu sql select thông tin user tạo dự án (#sql3.2):
                    ```sql
                    SELECT user.name, user.id, user.email
                    FROM user
                    where user.id = <user login>
                    ```
                    - Template gửi mail bao gồm:
                        - Tên user: (#sql3.1)
                        - Tên người tạo dự án (#sql3.2)
                        - Thông tin dự án: tên, vai trò, ngày bắt đầu, ngày kết thúc (Thông tin ở bước 1)

    - Trạng thái xử lý:
        - Thành công => redirect về màn hình danh sách dự án. 
        - Thất bại => hiển thị thông báo. Ngưng xử lý