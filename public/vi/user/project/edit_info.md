
# User - Project - Edit info of Project

- **Quyền truy cập:** User thuộc dự án.
- Xem và chỉnh sửa thông tin

## Di chuyển màn hình

```mermaid
graph LR
A[Overview] --Click vào status--> B[Edit info của Project]
B --Quay lại--> A
```
## Logic
- Nếu user đăng nhập là người tạo dự án => toàn quyền.
- Nếu user đăng nhập chỉ là user của dự án => chỉ xem.

- Hiển thị thông tin.
    - Câu sql
        ```sql
        SELECT id, name, description, start_date, end_date, status, created_by, created_at, user_profile.name
        FROM project
        LEFT JOIN user_profile
        ON  project.created_by  =  user_profile.user_id
        where project_id = [param project id]
        ```
    - Hiển thị

    |**Input fill** |**Input type**   | **Enable/Disable** | **Required**|**DB** |**Validate**|
    |----|------------|---|---|-------|------------|
    |Tên | Text box |Enable| O |  project.name| Text (225) / Unique |
    |Ngày bắt đầu | Date |Enable| O |  project.start_date| date (Y-m-d) |
    |Ngày kết thúc | Date |Enable| O |  project.end_date| date (Y-m-d), lớn hơn Ngày bắt đầu |
    |Mô tả | CKEditor |Enable| O |  project.description| Text |
    |Trạng thái | SelectBox |Enable| O |  project.status|  |
    |Người tạo | Label |Enable| O |  user_profile.name|  |
    - `project.name`: Textbox hiển thị giá trị
    - `project.start_date`: Textbox hiển thị giá trị
    - `project.end_date`: Textbox hiển thị giá trị
    - `project.description`: CKEditor hiển thị giá trị
    - `project.status`: Select hiển thị giá trị. Option thuộc mảng ['not_started' => 'not_started', 'in_progress' => 'in_progress', 'pending' => 'pending', , 'completed' => 'completed'].
    - Nếu user đăng nhập chỉ là user của dự án => disabled các input

- Cập nhật thông tin: Nếu user đăng nhập chỉ là user của dự án => ẩn nút này
    - Câu sql:
     ```sql
    UPDATE project
    SET name = [input name], description = [input description], start_date = [input start_date], end_date = [input end_date], status = [input status], updated_at = now
    WHERE id = [input id];
    ```