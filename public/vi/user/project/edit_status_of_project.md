
# User - Project - Edit Status of Project

- **Quyền truy cập:** User thuộc dự án.
- Danh sách status của dự án.
- Thêm, xóa và sửa thông tin status.

## Di chuyển màn hình

```mermaid
graph LR
A[Overview] --Click vào status--> B[Edit Status của Project]
B --Quay lại--> A
```

## Logic
- Nếu user đăng nhập thuộc role ['project_manager', 'project_leader'] => toàn quyền.
- Nếu user đăng nhập khác role ['project_manager', 'project_leader'] => chỉ hiển thị danh sách để xem.

- Hiển thị danh sách trạng thái task.
    - Câu sql
        ```sql
        SELECT id, name, position
        FROM project_task_status
        where project_id = [param project id]
            and deleted_at = null
        ORDER BY position asc
        ```
    - Hiển thị
        | STT |Tên |Vị trí||
        |------|-----|-----|-----|
        | 1 |Draft|1||
        | 2 |New|2||
        | 3 |Đang làm|2|[btn_delete]|
        | 4 |Đợi test|3|[btn_delete]|
        ...
        | 5 |Closed|end||
        | |`project_task_status.name`|`project_task_status.position`|`[btn_delete]`|
        - `project_task_status.name`: Textbox hiển thị giá trị
        - `project_task_status.position`: Textbox hiển thị giá trị.(Hiện tại cho nhập giá trị version sau không cho nhập nữa mà kéo thả chọn vị trí)
        - `[btn_delete]`: Xóa Status đã chọn ra khỏi danh sách. Không xóa được các giá trị mặc định.
        - Các giá trị status là: 
            - Mặc định: Draft, New ở đầu tiên. Hiển thị lable không cho chỉnh sửa
            - `Các giá trị được thêm mới sẽ nằm ở khoảng giữa`.
            - Mặc định: Giá trị cuối cùng là Closed. Hiển thị lable không cho chỉnh sửa

        - Nút thêm mới: Add thêm 1 row vào danh sách trạng thái. (add vào vùng ở giữa các giá trị mặc định)
        - Validate mỗi row của danh sách:
   
- Nút thêm mới: Add thêm 1 row vào danh sách trạng thái
- Validate mỗi row của danh sách:

    |**Input fill** |**Input type**   | **Enable/Disable** | **Required**|**DB** |**Validate**|
    |----|------------|---|---|-------|------------|
    |Tên | Textbox |Enable| O |  project_task_status.name| Text (225) / Unique |
    |Vị trí| Textbox |Enable| O |  project_task_status.position| number|

- Nút cập nhật thông tin: chỉ lưu các giá trị được thêm => không thêm giá trị mặc định
    - Câu sql
        ```sql
        SELECT id, name, position
        FROM project_task_status
        where project_id = [param project id]
            and deleted_at = null
        ORDER BY position asc
        ```  
    - Kiểm tra danh sách cập nhật(CN) với danh sách trong BD(DB):
        - Nếu danh sách CN chưa có trong DB => thêm mới vào table với câu sql
            ```sql
            INSERT INTO project_task_status (name, position, project_id, created_at, updated_at)
            VALUES ([input name], [input position], <project_id ở bước 1>, <datetime now>, <datetime now>),
            VALUES ([input name], [input position], <project_id ở bước 1>, <datetime now>, <datetime now>)...;
            ```
        - Nếu danh sách DB có mà CN không có => xóa khỏi table với câu sql
            ```sql
            UPDATE project_task_status
            SET deleted_at = now
            WHERE id in [array_id_status];
            ```
        - Nếu danh sách CN có trong danh sách DB => cập nhật thông tin theo từng status
            ```sql
            UPDATE project_task_status
            SET name = [input name], position = [input position], updated_at = now
            WHERE id = [input id];
            ```

   