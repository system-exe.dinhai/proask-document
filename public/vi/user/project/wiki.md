# ProjectWiki List
- Hiển thị toàn bộ **ProjectWiki** của **Project**
- Tìm kiếm **ProjectWiki** (onKeyup)
- Chức năng cho phép **Thêm mới** một **ProjectWiki** vào page **wiki**
- Chỉnh sửa một **ProjectWiki**
- Xóa một **ProjectWiki**

## Di chuyển Màn hình

```mermaid
graph LR
L[Wiki List] -- 'chọn xem 1 ProjectWiki' --> A[Show ProjectWiki] --'đóng ProjectWiki'--> L
```

## Logic

|**Input fill** |**Input type**   | **Enable/Disable** | **Required**|
|----|------------|---|---|
|Search | Input |Enable| X |  

### Button

|Tên Button  |Điều Kiện Hiện  |
|--|--|
|  Add Wiki| Luôn hiện |
|  (icon delete)| nằm bên trong mỗi ProjectWiki |

###  Thêm một **ProjectWiki** 

- Đang ở màng hình **Wiki**
- Click btn **Add Wiki**
- Hiện một **ProjectWiki** trắng, nếu **ProjectWiki** vừa hiện có **data**, thêm mới **ProjectWiki**.
	```sql
		INSERT INTO ProjectWiki (title, description, priority, project_id, created_by, created_at, updated_at)
		VALUES ('ProjectWiki Title', 'ProjectWiki Description', 'medium', 1, 2, NOW(), NOW());
	```

###  Tìm kiếm **ProjectWiki** 

- Đang ở màng hình **Wiki**
- Nhập vào input search, search onKeyup theo **data** trong input
	- Nếu tìm có **ProjectWiki**, show **ProjectWiki** lên màng hình.
	- Nếu không có một **ProjectWiki** nào hết, show text thông báo **'Không tìm thấy kết quả'**

	```sql
		SELECT * FROM ProjectWiki 
		WHERE (title LIKE '%param%' OR description LIKE '%param%')
	```

###  Sửa một **ProjectWiki** 

- Đang ở màng hình **Wiki**
- Click chọn xem một **ProjectWiki** 
- Thay đổi **data** trong  **ProjectWiki** 
- Click ra ngoài popup của **ProjectWiki**, thay đổi **data**, tắt popup
	```sql
		UPDATE ProjectWiki
		SET title = IFNULL(:title, title),
			description = IFNULL(:description, description),
			updated_at = NOW()
		WHERE id = :id;
	```

	
	- Click **Không**, tắt popup, màng hình trở lại trang **wiki**.

###  Xóa một **ProjectWiki** 

- Đang ở màng hình **Wiki**
- Click chọn xem một **ProjectWiki** 
- Click **icon delete** của **ProjectWiki**, Hiện popup **'Bạn có muốn xóa wiki này!'**:
	- Click **Có**, xóa **ProjectWiki**.

	```sql
		DELETE FROM ProjectWiki WHERE id = <id>;
	```

	
	- Click **Không**, tắt popup, màng hình trở lại trang **wiki**.