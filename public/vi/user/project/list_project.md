
## User - Project - List Project

### Transition flow

```mermaid
graph LR
A[Dashboard]
C[Project Edit]
D[Project Overview]
P[List Project]
A--'Click Main Menu'-->P
P--'Back'-->A
P--'Click item'-->C
P--'Click item'-->D
C--'Back'-->P
C--'Back'-->D
D--'Back'-->P
D--'Click'-->C
```

### Search Project

#### Mô tả: 

- Thực hiện search danh sách project theo những mục tiêu mong muốn nhất định
- Chỉ hiển thị những project mà user đã tạo hoặc user thuộc project đó

#### Logic: 
- Search project theo key input 

|Item|Type|Search|Validate|Table|Column|Note|
|----|----|----|----|----|----|----|
|PROJECT NAME|Input|Like|(Note)|projects|name|(Note)|
|STATUS|Checkbox|Bằng|(Note)|projects|status|Hiển thị danh sách Status dưới dạng checkbox|
|CREATED|Date time|Like|Ngày From nhỏ hơn ngày To|projects|created_at|hiển thị 2 input From - To|
|CREATE BY| Input|Like|(Note)|user_profiles|name|(Note)|
|START DAY| Date time|Like|Ngày From nhỏ hơn ngày To|projects|start_date|hiển thị 2 input From - To|
|END DAY|Date time|Like|Ngày From nhỏ hơn ngày To|projects|end_date|hiển thị 2 input From - To|

- Kết quả search sắp xếp giảm dần theo ngày tạo mới
- Trường hợp search không có data:
	- Hiển thị text thông báo: Không tồn tại project
	```
	SELECT *
	FROM projects
	ORDER BY ASC projects.created_at
	```
- Trường hợp search Ngày CREATED To nhỏ hơn From:
	- Hiển thị text validate: Ngày Form phải nhỏ hơn To
- Phân trang: 10 project per page 
	- Hiển thị selectbox phân trang gồm các item: 10, 50, 100
- Hiển thị tổng số lượng record
- Hiển thị button:
	- Search 
		- Thực hiện search
	- Clear  
		- Xóa những giá trị đã được nhập

### Hiển thị list project

#### Mô tả: 
- Hiển thị danh sách project

#### Logic:
- Hiển thị danh sách project giảm dần theo ngày tạo mới  
- Trường hợp không có data:
	- Hiển thị text thông báo: Không tồn tại user
- Nếu project đã được delete: Không hiển thị (project có `'projects.[deleted_at] != NULL'`)
		```
		SELECT *
		FROM projects
		WHERE projects.deleted_at != NULL
		ORDER BY ASC projects.created_at
		```
- STATUS của PROJECT: thành từng label nhỏ cạnh PROJECT NAME theo màu sắc
	vd:
	- pending: màu vàng
	- completed: màu xanh đậm
	- in_progress: màu xanh nhợt
	- not_started: màu xám
- Phân trang: 10 project per page 
	- Hiển thị selectbox phân trang gồm các item: 10, 50, 100
- Hiển thị tổng số lượng record
- Hiển thị button:
	- Create Project

### Kết quả mong muốn

|NO|PROJECT|DATE||
|----|----|----------|----------|
|1|TEST A `'pending'` <br>CREATE BY: Thạch Anh Tuấn<br>CREATED: 02/01/2023 06:54|02/02/2023 06:54 ~ 14/03/2023 06:54| [ICON PENCIL](http://link_page_edit_thông_tin_project) <br>[ICON EYE](http://link_page_view_thông_tin)
|2|Owner MyPage `'completed'` <br>CREATE BY: Thạch Anh Tuấn<br>CREATED: 02/01/2023 06:54 |02/03/2021 06:54 ~ 02/03/2022 06:54| [ICON PENCIL](http://link_page_edit_thông_tin_project) <br>[ICON EYE](http://link_page_view_thông_tin)
|3|ProAsk `'in_progress'` <br>CREATE BY: Châu Lâm Đình Ái<br>CREATED: 02/01/2023 06:54| 16/02/2023 06:54 ~ 16/05/2023 06:54| [ICON PENCIL](http://link_page_edit_thông_tin_project) <br>[ICON EYE](http://link_page_view_thông_tin)
|4|TEST B `'not_started'` <br>CREATE BY: Thạch Anh Tuấn<br> CREATED: 02/01/2023 06:54 | 16/02/2023 06:54 ~ 16/05/2023 06:54| [ICON PENCIL](http://link_page_edit_thông_tin_project) <br>[ICON EYE](http://link_page_view_thông_tin)
||`'projects.[name]'` <br>`'project_task_status.[name] WHERE projects.[ID] == project_task_status.[ID]'` <br>`'user_profiles.[name] WHERE projects.[created_by] == users.[ID] AND user_profiles.[user_id] == users.[ID]'`<br>`'projects.[created_at]'` | `'projects.[start_date]'` ~ `'projects.[end_date]'`| |




## Admin - Project - List Project

### Search Project

#### Mô tả: 

- Thực hiện search danh sách project theo những mục tiêu mong muốn nhất định

#### Logic: 
- Search project theo key input 

|Item|Type|Search|Validate|Table|Column|Note|
|----|----|----|----|----|----|----|
|PROJECT NAME|Input|Like|(Note)|projects|name|(Note)|
|STATUS|Checkbox|Bằng|(Note)|projects|status|Hiển thị danh sách Status dưới dạng checkbox|
|CREATED|Date time|Like|Ngày From nhỏ hơn ngày To|projects|created_at|hiển thị 2 input From - To|
|CREATE BY| Input|Like|(Note)|user_profiles|name|(Note)|
|START DAY| Date time|Like|Ngày From nhỏ hơn ngày To|projects|start_date|hiển thị 2 input From - To|
|END DAY|Date time|Like|Ngày From nhỏ hơn ngày To|projects|end_date|hiển thị 2 input From - To|
|TIMELINE|Date time|Like|Ngày From nhỏ hơn ngày To|project_timelines|time_release|hiển thị 2 input From - To|

### Hiển thị list project

#### Mô tả: 
- Hiển thị danh sách project

#### Logic:
- USER: số lương user của project
		```
		SELECT COUNT(DISTINCT user_id)
		FROM project_users
		WHERE project_id = <project_id> AND deleted_at IS NULL
		```
- TASKS: số lượng task của project
	```
	SELECT COUNT(id)
	FROM tasks
	WHERE project_id = <project_id> AND deleted_at IS NULL
	```
	- Trường hợp task bị trễ hiển thị: 
	Task trễ tiến độ: <số lượng>
	```
	SELECT COUNT(id)
	FROM tasks
	JOIN projects ON tasks.project_id = projects.id
	JOIN project_task_status ON tasks.project_task_status_id = project_task_status.id
	JOIN task_types ON tasks.task_type_id = task_types.id
	LEFT JOIN merge_requests ON tasks.gitlab_merge_request_id = merge_requests.id
	LEFT JOIN design_details ON tasks.design_detail_id = design_details.id
	JOIN project_users ON tasks.project_id = project_users.project_id AND tasks.created_by = project_users.user_id
	JOIN users ON project_users.user_id = users.id
	WHERE tasks.end_time < NOW() AND tasks.is_close = 0 AND projects.id = <project_id>
	```
- TIMELINE: hiển thị timeline
		```
		SELECT project_timelines.name, project_timelines.time_release
		FROM project_timelines
		WHERE project_id = <project_id> AND deleted_at IS NULL;
		```		
### Kết quả mong muốn

|NO|PROJECT|TASKS|DATE|TIMELINE||
|----|----|----|----|----|----|
|1|TEST A `'pending'`  <br> USERS: 10<br>CREATE BY: Thạch Anh Tuấn<br>CREATED: 02/01/2023 06:54|Tổng task: 20|02/02/2023 06:54 ~ 14/03/2023 06:54|Phase 1: 02/02/2023 ~ 29/02/2023 <br> Phase 2: 29/02/2023 ~ 14/03/2023| [ICON PENCIL](http://link_page_edit_thông_tin_project) <br>[ICON EYE](http://link_page_view_thông_tin)
|2|Owner MyPage `'completed'`  <br> USERS: 12 <br>CREATE BY: Thạch Anh Tuấn<br>CREATED: 02/01/2023 06:54|Tổng task:100| 02/03/2021 06:54 ~ 02/03/2022 06:54|Phase 1: 02/02/2023 ~ 29/02/2023 <br> Phase 2: 29/02/2023 ~ 14/03/2023| [ICON PENCIL](http://link_page_edit_thông_tin_project) <br>[ICON EYE](http://link_page_view_thông_tin)
|3|ProAsk `'in_progress'`<br> USERS: 10<br>CREATE BY: Châu Lâm Đình Ái  <br>CREATED: 02/01/2023 06:54 |Tổng task: 300 <br> Task trễ tiến độ: 10| 16/02/2023 06:54 ~ 16/05/2023 06:54|Phase 1: 02/02/2023 ~ 29/02/2023 <br> Phase 2: 29/02/2023 ~ 14/03/2023| [ICON PENCIL](http://link_page_edit_thông_tin_project) <br>[ICON EYE](http://link_page_view_thông_tin)
|4|TEST B `'not_started'`<br> USERS: 20  <br>CREATE BY: Thạch Anh Tuấn <br> CREATED: 02/01/2023 06:54| Tổng task: 200|16/02/2023 06:54 ~ 16/05/2023 06:54|Phase 1: 02/02/2023 ~ 29/02/2023 <br> Phase 2: 29/02/2023 ~ 14/03/2023| [ICON PENCIL](http://link_page_edit_thông_tin_project) <br>[ICON EYE](http://link_page_view_thông_tin)
||`'projects.[name]'`<br> `'project_task_status.[name] WHERE projects.[ID] == project_task_status.[ID]'`<br>  `'SELECT COUNT(DISTINCT user_id) FROM project_users'` <br>`'user_profiles.[name] WHERE projects.[created_by] == users.[ID] AND user_profiles.[user_id] == users.[ID]'` <br>`'projects.[created_at]'` |`'SELECT COUNT(id) FROM tasks WHERE project_id = <project_id> AND deleted_at IS NULL'`| `'projects.[start_date]'` ~ `'projects.[end_date]'`|`'SELECT name, time_release FROM project_timelines'`||

