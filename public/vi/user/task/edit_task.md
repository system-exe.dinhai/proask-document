
# User - Task - Edit Task

- **Quyền truy cập:** Mọi user thuộc project
- Chỉnh sửa thông tin liên quan đến task

## Di chuyển màn hình

```mermaid
graph LR
A[Detail Task] --Click edit--> B[Edit task]
B --Quay lại--> A
```

## Logic
- ### Màn hình edit task 
    - Câu sql select task
        - Câu sql lấy thông tin task
         ```sql
        SELECT task.name, task.id, task.description, task.start_time, task.end_time, task.is_draft, task.is_new, task.is_close,task.percent_process, task.priority, project_task_status.name as status_name, task_type.name as type_name, user_profile.name as assign_name, project.name as project_name, create_by.name as user_create_by_name, task.created_at
        FROM task
        where task.id = [id task]
            and task.deleted_at = null
        ```
    - Chỉnh sửa task bao gồm các thông tin

        |**Input fill** |**Input type**   | **Enable/Disable** | **Required**|**DB** |**Validate**|
        |----|------------|---|---|-------|------------|
        |Tên | Text box |Enable| O |  task.name| Text (225) / Unique |
        |Ngày bắt đầu | Datetime |Enable|  |  task.start_date| date (Y-m-d) |
        |Ngày kết thúc | Datetime |Enable|  |  task.end_date| date (Y-m-d), lớn hơn Ngày bắt đầu |
        |% Hoàn thành | SelectBox |Enable|  |  task.precent_process| number |
        |Priority | SelectBox |Enable| O |  task.priority|  |
        |Status | SelectBox |Enable| O |  task.project_task_status_id|  |
        |Type | SelectBox |Enable| O |  task.task_type_id|  |
        |Assign | SelectBox |Enable|  |  task_assign_user.user_id| Nếu ko chọn default là ng tạo |
        |Mô tả | CKEditor |Enable|  |  task.description| Text |
        |Tags | textboxSelect |Enable|  |  project_tag.project_tag_name_id| Text |
        | |  |  | |  task.created_by = user login| |

        - Giá trị các **SelectBox**
            - `Tên`: Hiển thị giá trị
            - `Priority`: giá trị thuộc mảng ['low', 'medium', 'high']
            - `Status`: giá trị thuộc mảng ['draft' => Nháp, 'new' => New]
            - `Type`: Chọn giá trị type cho task
                - Câu sql:
                    ```sql
                    SELECT name, id
                    FROM TaskType
                    where deleted_at = null
                    ```
                - Giá trị hiển thị là name, value là id
            - `Assign`: Chọn giá trị assign cho task là user thuộc dự án được chọn
                - Câu sql:
                ```sql
                    SELECT user_profile.name, user_profile.user_id
                    FROM project_user
                    JOIN user_profile
                        ON project_user.user_id = user_profile.user_id
                    where project_task_status.project_id = [param project id được chọn]
                        AND deleted_at = null
                    ```
            - `Tags`: TextBox cho nhập nhiều tag. keyUp tìm kiếm trong bản project_tag_name. 
                - Nếu có thì hiển thị ở bên dưới cho phép chọn tag.
                - Nếu không => hiển thị tag bình thường.
                - Câu sql tìm kiếm:
                ```sql
                    SELECT name, id
                    FROM project_tag_name
                    where project_tag_name.project_id = [param project id được chọn]
                        AND name like %[param tag tìm kiếm]%
                        AND deleted_at = null
                    ```
    - Button Cập nhật
        - Câu sql cập nhật task (sql#1)
            ```sql
            UPDATE task
            SET name = [input name],
                description = [input description], 
                start_time = [input start_time],
                end_time = [input end_time], 
                is_draft = [Nếu status chọn là draft => 1 : 0]
                is_new = [Nếu status chọn là new => 1 : 0]
                is_close = [Nếu status chọn là closed => 1 : 0]
                priority = [input priority]
                project_task_status_id = [Nếu status chọn thuộc [draft, new, closed] => null : giá trị status chon]
                task_type_id = [input type]
                updated_at = <datetime now>
            WHERE id = [param task id];
             ```
        - Câu sql update assign user(#sql2)
            - Nếu ko chọn user assign => giá trị user assign là user login
             ```sql
                UPDATE task_assign_user
                SET task_id = [param task id],
                    user_id = [giá trị user assign], 
                    assigned_at = <datetime now>,
                    updated_at = <datetime now>,
                WHERE id = [input id];
             ```
        - Câu sql add tag of task
            - Câu sql xóa tất cả tag đang có của task
                ```sql
                DELETE FROM project_tag WHERE project_tagable_id = [param task id] AND project_tagable_type = 'task';
                ```
            - Câu sql tìm kiếm các tag của project theo tag của task(#sql3)
                ```sql
                    SELECT name, id
                    FROM project_tag_name
                    where project_tag_name.project_id = [param project id được chọn]
                        AND name in [array tag]
                        AND deleted_at = null
                ```
            - Kiểm tra nếu tag của task chưa tồn tại trong mảng ở câu #sql3 => thêm mới
             ```sql
                INSERT INTO project_tag_name (name, project_id, created_at, updated_at)
                VALUES (
                    [input name tag], 
                    [param project id được chọn], 
                    <datetime now>, 
                    <datetime now>
                );
             ```
            - Thêm mới tag of task
                ```sql
                INSERT INTO project_tag (project_tagable_id, project_tagable_type, project_tag_name_id, created_at, updated_at)
                VALUES (
                    [id của task (#sql1)], 
                    'task', 
                    [id của project_tag_name], 
                    <datetime now>, 
                    <datetime now>
                );
                ```
            - Thêm mới log
                - Nếu thông tin task ko có sự thay đổi => không thực hiện thêm log
                - Nếu thông tin task có sự thay đổi => thực hiện thêm log
                    - Thêm 1 record mới vào bảng task_change_log (câu #sql4):
                        ```sql
                        INSERT INTO task_change_logs (task_id, created_by, created_at, updated_at)
                        VALUES (
                            [id của task (#sql1)], 
                            [user login], 
                            <datetime now>, 
                            <datetime now>
                        );
                         ```
                    - Kiểm tra sự thay đổi của thông tin task theo column. `Mỗi sự thay đổi là thêm 1 row với state_old và state_new tương ứng` .Ví dụ:
                        - Thay đổi name:
                            ```sql
                            INSERT INTO task_change_log_detail (task_change_log_id, type, state_old, state_new, created_at, updated_at)
                            VALUES (
                                [id của task_change_logs (#sql4)], 
                                'properties', 
                                '{"field": "name", "value":"[Giá trị trước khi thay đổi]"}'
                                '{"field": "name", "value":"[Giá trị được thay đổi]"}'
                                <datetime now>, 
                                <datetime now>
                            );
                            ```
                        - Thay đổi status: 
                            - Nếu column `is_draft` or `is_new` or `is_close` or `project_task_status_id` có sự thay đổi.
                                ```
                                    //task có giá trị trước khi thay đổi
                                    valueOld = null;
                                    project_task_status_id = task.project_task_status_id
                                    valueNew = [input project_task_status_id] // giá trị thay đổi
                                    arrayDefault = [`draft`, `new`, `closed`]

                                    if (project_task_status_id != null) {
                                            valueOld = task.project_task_status_id
                                    } else {
                                        if (task.is_draft == 1) valueOld = draft
                                        if (task.is_new == 1) valueOld = new
                                        if (task.is_close == 1) valueOld = closed
                                    }
                                    
                                ```
                                
                            - Giá trị old: 
                            ```
                                {
                                    "field": "project_task_status_id", "value":"[valueOld xét ở trên]"
                                }
                            ```
                            - Giá trị new: 
                            ```
                                {
                                    "field": "project_task_status_id", "value":"[input project_task_status_id truyền lên]"
                                }
                            ```
                        - Các column khác tương tự nhuư colum name
        - Trạng thái xử lý:
            - Thành công => redirect về màn hình detail task được chọn. 
            - Thất bại => hiển thị thông báo. Ngưng xử lý
- ### Add task in group
    - Nút chọn group => hiển thị modal chứa danh sách các group
        - Câu sql get list group
        ```sql
            SELECT task_group.name, task_group.id
            FROM task_group
            where task_group.project_id = [id task]
                and task_group.deleted_at = null
        ```
        - Câu sql task thuộc group
         ```sql
            SELECT user_task_group.task_group_id
            FROM user_task_group
            where user_task_group.task_id = [id task]
                and task.deleted_at = null
            ```
    - Hiển thị danh sách các group task hiện tại đã có từ 2 câu sql trên.
    - Chọn checkbox trên từng task để add
    - Bấm nút save => thực hiện câu sql để lưu task group đã chọn.