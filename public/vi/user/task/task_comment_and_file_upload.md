
# User - Task - Task Comment and File Upload

- **Quyền truy cập:** Mọi User đều có quyền
- Comment:
    - Comment được tạo mới/update/delete khi Task được **edit**
    - Quyền edit và delete:
        - **User tạo comment**: edit, delete comment của chính mình
        - **Admin**: KHÔNG edit, delete comment
- File Upload:
    - File được tạo mới/update/delete khi Task được **edit**
    - File được tạo mới/update/delete khi comment được **tạo mới/edit**
    - Quyền edit và delete:
        - **User upload File**: edit, delete File của chính mình
        - **Admin**: KHÔNG edit, delete File 
    - Quyền download: Mọi User

## Task Comment
### Logic
#### Comment được tạo mới/update/delete khi Task được **edit**

- Trường hợp tạo mới/edit Task nhưng không thực hiện commnet: Không insert DB
- Trường hợp tạo mới/edit Task và có thực hiện tạo mới/update/delete comment: insert DB vào table `task_change_log` và `task_change_log_detail`
    - Câu SQL insert vào table `task_change_log` và `task_change_log_detail` khi **tạo mới comment**:

        ```sql
        INSERT INTO task_change_log (task_id, created_by, created_at, updated_at)
        VALUES (<task_id>, <user_id>, NOW(), NOW());
        ```
        ```sql
        INSERT INTO task_change_log_detail (task_change_log_id, type, state_old, state_new, created_at, updated_at)
        VALUES (
            <task_change_log_id>, 
            <comment>, 
            [], 
            [{
                "field":"comment", 
                "value":"comment (text)"}], 
            NOW(), 
            NOW()
        );
        ```
    - Câu SQL insert vào table `task_change_log` và `task_change_log_detail` khi **update comment**:
        ```sql
        INSERT INTO task_change_log (task_id, created_by, created_at, updated_at)
        VALUES (<task_id>, <user_id>, NOW(), NOW());
        ```
        ```sql
        INSERT INTO task_change_log_detail (task_change_log_id, type, state_old, state_new, created_at, updated_at)
        VALUES (    
            <task_change_log_id>, 
            <comment>, 
            [{"field": "comment", "value":"old comment (text)"}], 
            [
                {"field": "comment", "value":"old comment (text)"}, 
                {"field": "comment", "value":"new comment (text)"}
            ],
            NOW(), 
            NOW()
        );
        ```
    - Câu SQL insert vào table `task_change_log` và `task_change_log_detail` khi **delete comment**:
        ```sql
        INSERT INTO task_change_log (task_id, created_by, created_at, updated_at)
        VALUES (<task_id>, <user_id>, NOW(), NOW());
        ```
        ```sql
        INSERT INTO task_change_log_detail (task_change_log_id, type, state_old, state_new, created_at, updated_at, deleted_at)
        VALUES (
            <task_change_log_id>, 
            <comment>, 
            [{"field": "comment", "value":"old comment (text)"}], 
            [{"field": "comment", "value":"new comment (text)"}], 
            NOW(), 
            NOW(), 
            NOW()
        );
        ```
    - Trong đó, giá trị của các trường như sau:
        - `<task_id>`: ID của Task được tạo mới/edit
        - `created_by`: `<user_id>`: ID của User thực hiện tạo mới/edit Task
        - `state_old`: nội dung json chứa thông tin comment trước khi chỉnh sửa 
        - `state_new`: nội dung json chứa thông tin comment sau khi chỉnh sửa

## File Upload
### Logic
#### File được tạo mới/update/delete khi Task được edit và khi comment được tạo mới/edit

- Trường hợp Task được edit và khi comment được tạo mới/edit nhưng không thực hiện upload file: Không insert DB
- Trường hợp Task được edit và khi comment được tạo mới/edit và thực hiện tạo mới/update/delete file: insert DB vào table `task_change_log` và `task_change_log_detail`
    - Câu SQL insert vào table `task_change_log` và `task_change_log_detail` khi **tạo mới 1 file**:
        ```sql
        INSERT INTO task_change_log (task_id, created_by, created_at, updated_at)
        VALUES (<task_id>, <user_id>, NOW(), NOW());
        ```
        ```sql
        INSERT INTO task_change_log_detail (task_change_log_id, type, state_old, state_new, created_at, updated_at)
        VALUES (
            <task_change_log_id>, 
            <properties>, 
            [], 
            [{
                "field":"file_upload", 
                "value":"https://system-exe.dinhai.gitlab.io/proask-pj/attachments/download/3028/test0.png"}], 
            NOW(), 
            NOW()
        );
        ```
    - Câu SQL insert vào table `task_change_log` và `task_change_log_detail` khi **update 1 file**:
        ```sql
        INSERT INTO task_change_log (task_id, created_by, created_at, updated_at)
        VALUES (<task_id>, <user_id>, NOW(), NOW());
        ```
        ```sql
        INSERT INTO task_change_log_detail (task_change_log_id, type, state_old, state_new, created_at, updated_at)
        VALUES (    
            <task_change_log_id>, 
            <properties>, 
            [{
                "field":"file_upload", 
                "value":"https://system-exe.dinhai.gitlab.io/proask-pj/attachments/download/3028/test0.png"
            }], 
            [
                {
                    "field": "file_upload", 
                    "value":"https://system-exe.dinhai.gitlab.io/proask-pj/attachments/download/3028/test0.png",
                    "name":"A"
                }, 
            ],
            NOW(), 
            NOW()
        );
        ```
    - Câu SQL insert vào table `task_change_log` và `task_change_log_detail` khi **delete 1 file**:
        ```sql
        INSERT INTO task_change_log (task_id, created_by, created_at, updated_at)
        VALUES (<task_id>, <user_id>, NOW(), NOW());
        ```
        ```sql
        INSERT INTO task_change_log_detail (task_change_log_id, type, state_old, state_new, created_at, updated_at)
        VALUES (
            <task_change_log_id>, 
            <properties>, 
            [
                {
                    "field": "file_upload", 
                    "value":"https://system-exe.dinhai.gitlab.io/proask-pj/attachments/download/3028/test0.png"
                    "name":"A",
                }, 
            ],
            [
                {
                    "field": "file_upload", 
                    "value":"https://system-exe.dinhai.gitlab.io/proask-pj/attachments/download/3028/test0.png",
                    "name":"A",
                    "is_deleted":"true"
                }
            ],
            NOW(), 
            NOW()
        );
        ```
    - Trường hợp **upload nhiều file** thì sẽ lưu thành **nhiều record** vào table `task_change_log` và `task_change_log_detail`. Trong đó, giá trị của các trường như sau:
        - `<task_id>`: ID của Task được tạo mới/edit
        - `created_by`: `<user_id>`: ID của User thực hiện tạo mới/edit Task/comment
        - `state_old`: nội dung json chứa thông tin File trước khi chỉnh sửa 
        - `state_new`: nội dung json chứa thông tin File sau khi chỉnh sửa
