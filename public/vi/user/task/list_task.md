
## User - Project - List Task

- Show toàn bộ **task** trong project
- Phân trang
- Gom nhóm 3 loại task(**priority**) là **high, medium, low** và có **end_time** tăng dần
- Thứ tự show trên Table List lần lượt là **high, medium, low**
- Search task theo: **user, priority, Status, TaskType, start_time, end_time**

### Transition flow

```mermaid
graph LR
A[Dashboard]
B[Task Detail]
C[Task Create]
D[Task Delete]
P[List Task]
A--'Click Main Menu'-->P
P--'Back'-->A
P--'Click Create'-->C
C--'Back'-->P
P--'Click iconEdit'-->B
B--'Back'-->P
P--'Click iconDelete'-->D
D--'Close popup'-->P
```

### Button

|Tên Button  |Điều Kiện Hiện  | Actione |
|--|--|--|
|  Search| Luôn hiện | Thực hiện chức năng search |
|  Clear Search| Luôn hiện | xóa các field trong form search |
|  Create| Luôn hiện | Link sang trang Create Task |
|  iconEdit| Luôn hiện | Link sang trang editTask |
|  iconUp| Luôn hiện | Sort DESC(tăng) |
|  iconDown| Luôn hiện | Sort ASC (giảm) |
|  priority(Badge) | Luôn hiện | Sort ASC (giảm) |
|  status(Badge) | Luôn hiện | Sort ASC (giảm) |

### List Task

- Đang ở màng hình TaskList, gọi API getListTask
	- Nếu không lỗi, trả về **code 200 và data**
		- Nếu có data, show data, phân trang trở lại **trang đầu tiên**
		- Nếu không có data show text **'không có dữ liệu'**
	- Nếu có lỗi, trả về **code 416 và data**, Redirect sang màng hình **404**

Select **$listTask**

```sql

	SELECT task.priority, task.name, task.is_draft, task.is_new, task.is_close, task.start_time, task.end_time,
	user_profile.name as user_name, task_type.name as task_type
	FROM task
		JOIN project_task_status ON project_task_status.project_id = task.project_id
		JOIN task_assign_user ON task.id = task_assign_user.task_id
		JOIN TaskType ON task_type.id = task.id
		JOIN user ON task_assign_user.user_id = user.id
		JOIN user_profile ON user.id = user_profile.user_id
	WHERE [
		task.deleted_at IS NULL
	]
	ORDER BY task.end_time ASC;
```

#### Search Task

- Đang ở màng hình TaskList, nhập data vào cách input, dateTimePicker... trong phần Search, Click **btn Search**
	- Nếu không lỗi, trả về **code 200 và data**
		- Nếu có data, show data, phân trang trở lại **trang đầu tiên**
		- Nếu không có data show text **'không có dữ liệu'**
	- Nếu lỗi do API, trả về **code 400 và data**

- Search theo user
	```
		where user.name = :name
	```

- Search theo priority
	```
		where task.priority = :priority
	```

- Search theo Status
	- nếu có status custom
	```
		where task.priority = :priority
	```
	- nếu ko có status custom sẽ lấy theo ưu tiên **is_close, is_new, is_draft**
	
	```
		Kiểm tra status nếu:
			if (is_close) {
				where [task.is_close = true]
			} else if (is_new) {
				where [task.is_new = true]
			} else {
				where [task.is_draft = true]
			}
		
	```

- Search theo TaskType
	```
		where task_type.name = :task_type
	```

- Search theo Time
	```
		where between task.start_time to task.end_time
	```

- Search user theo key input

|Item|Type|Search|Table|Column|Note|
|----|----|----|----|----|----|
|NAME|Input|Like|user|name|(Note)|
|priority|Select|Like|task|priority|get all priority rồi show ra|
|Status|Select|Like|task, project_task_status||get all Status rồi show ra, gồm cả default và custom|
|TaskType|Select|Like|task|task_type|get all task_type rồi show ra|
|TIME|Date time|Like|task|start_time, end_time|hiển thị 2 start_time - end_time|

#### Kết quả mong muốn


Ví dụ hôm nay là: 15h ngày 24/01/2023

|#|TaskName|UserName|start_time [iconUp, iconDown]|end_time [iconUp, iconDown]|Done [iconUp, iconDown]|Action|
|--|--|--|--|--|--|--|
|3|<span style="color: red">Thay text  [Low - New] (Badge)</span> | <span style="color: red">Tân 2k5</span> | <span style="color: red">Start: 2023-01-24</span>| <span style="color: red">End: 2023-01-24</span>| <span style="color: red">30%</span> |iconEdit|
|1|<span style="color: red">Không đổi được password [High - New] (Badge)</span> | <span style="color: red">Kimừ sàn</span>  |<span style="color: red">Start: 2023-01-22</span>| <span style="color: red">End: 2023-01-25</span>| <span style="color: red">33%</span> |iconEdit|
|2|<span style="color: red">Không hiện Toast thông báo  [High - New] (Badge)</span> | <span style="color: red">Tân 2k5</span> | <span style="color: red">Start: 2023-01-22</span>| <span style="color: red">End: 2023-01-25</span>| <span style="color: red">50%</span> |iconEdit|
|4|<span style="color: #C7A550">Page user chua co Validate  [Medium - New] (Badge)</span> | <span style="color: #C7A550">Hana Hana</span> |<span style="color: #C7A550">Start: 2023-01-22</span>| <span style="color: #C7A550">End: 2023-01-25</span>| <span style="color: #C7A550">70%</span> |iconEdit|
|5|bể layout trang login [High - New] (Badge) | Đức TN sàn | Start: 2023-01-24| End: 2023-01-25| 50% |iconEdit|
|.|.|.|.|.|.|.|
|.|.|.|.|.|.|.|
| `'task.id'`|`'task.name'` [`'task.priority'` `'task.status'`] | `'task.user_name'` | `'task.start_time'`| `'task.end_time'`|`'task.precent_process'`||


