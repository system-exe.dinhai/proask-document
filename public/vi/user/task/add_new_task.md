
# User - Task - Add New Task

- **Quyền truy cập:** Mọi user thuộc project
- Thêm mới thông tin task thuộc dự án đang chọn

## Di chuyển màn hình

```mermaid
graph LR
A[Overview Project] --Click tạo task--> C[Add new task]
B[Task list of Project] --Click tạo task--> C[Add new task]
D[Task list of User] --Click tạo task--> C[Add new task]
C --Quay lại--> B
```

## Logic
- ### Màn hình tạo mới task 
    - Thêm mới task bao gồm các thông tin

        |**Input fill** |**Input type**   | **Enable/Disable** | **Required**|**DB** |**Validate**|
        |----|------------|---|---|-------|------------|
        |Project | SelectBox |Enable| O |  task.project_id|  Unique |
        |Tên | Text box |Enable| O |  task.name| Text (225) / Unique |
        |Ngày bắt đầu | Datetime |Enable|  |  task.start_date| dat  e (Y-m-d) |
        |Ngày kết thúc | Datetime |Enable|  |  task.end_date| date (Y-m-d), lớn hơn Ngày bắt đầu |
        |% Hoàn thành | SelectBox |Enable|  |  task.precent_process| number |
        |Priority | SelectBox |Enable| O |  task.priority|  |
        |Status | SelectBox |Enable| O |  task.project_task_status_id|  |
        |Type | SelectBox |Enable| O |  task.task_type_id|  |
        |Assign | SelectBox |Enable|  |  task_assign_user.user_id| Nếu ko chọn default là ng tạo |
        |Mô tả | CKEditor |Enable|  |  task.description| Text |
        |Tags | textboxSelect |Enable|  |  project_tag.project_tag_name_id| Text |
        | |  |  | |  task.created_by = user login| |

        - Giá trị các **SelectBox**
            - `Project`: 
                - Nếu chọn tạo task trong 1 project => ko hiển thị => giá trị mặc định là project đang chọn
                - Nếu chọn tạo task từ màn hình danh sách task của user
                    - Câu sql: hiển thị tất cả project mà user đang tham gia
                     ```sql
                    SELECT project.name, project.id
                    FROM project_user
                    JOIN project
                        ON project.id = project_user.project_id
                    where project_user.user_id = [user login]
                        and project_user.deleted_at = null
                        and project.deleted_at = null
                    ```
            - `Priority`: giá trị thuộc mảng ['low', 'medium', 'high']
            - `Status`: giá trị thuộc mảng ['draft' => Nháp, 'new' => New]
            - `Type`: Chọn giá trị type cho task
                - Câu sql:
                    ```sql
                    SELECT name, id
                    FROM TaskType
                    where deleted_at = null
                    ```
                - Giá trị hiển thị là name, value là id
            - `Assign`: Chọn giá trị assign cho task là user thuộc dự án được chọn
                - Câu sql:
                ```sql
                    SELECT user_profile.name, user_profile.user_id
                    FROM project_user
                    JOIN user_profile
                        ON project_user.user_id = user_profile.user_id
                    where project_task_status.project_id = [param project id được chọn]
                        AND deleted_at = null
                    ```
            - `Tags`: TextBox cho nhập nhiều tag. keyUp tìm kiếm trong bản project_tag_name. 
                - Nếu có thì hiển thị ở bên dưới cho phép chọn tag.
                - Nếu không => hiển thị tag bình thường.
                - Câu sql tìm kiếm:
                ```sql
                    SELECT name, id
                    FROM project_tag_name
                    where project_tag_name.project_id = [param project id được chọn]
                        AND name like %[param tag tìm kiếm]%
                        AND deleted_at = null
                    ```
    - Button Thêm mới
        - Câu sql thêm mới task (sql#1)
            ```sql
                INSERT INTO task (name, description, start_time, end_time, is_draft, is_new, is_close, priority, project_task_status_id, project_id, task_type_id, created_by, created_at, updated_at)
                VALUES (
                    [input name], 
                    [input description], 
                    [input start_time], 
                    [input end_time], 
                    [Nếu status chọn là draft => 1 : 0], 
                    [Nếu status chọn là new => 1 : 0], 
                    0,
                    [input priority], 
                    null, 
                    [param project id được chọn],
                    [input type], 
                    <user_login>, 
                    <datetime now>, 
                    <datetime now>
                );
             ```
        - Câu sql assign user(#sql2)
            - Nếu ko chọn user assign => giá trị user assign là user login
             ```sql
                INSERT INTO task_assign_user (role, task_id, user_id, assigned_at, created_at, updated_at)
                VALUES (
                    null, 
                    [giá trị id task ở câu #sql1], 
                    [giá trị user assign], 
                    <datetime now>, 
                    <datetime now>, 
                    <datetime now>
                );
             ```
        - Câu sql add tag of task
            - Câu sql tìm kiếm các tag của project theo tag của task(#sql3)
                ```sql
                    SELECT name, id
                    FROM project_tag_name
                    where project_tag_name.project_id = [param project id được chọn]
                        AND name in [array tag]
                        AND deleted_at = null
                    ```
            - Kiểm tra nếu tag của task chưa tồn tại trong mảng ở câu #sql3 => thêm mới
             ```sql
                INSERT INTO project_tag_name (name, project_id, created_at, updated_at)
                VALUES (
                    [input name tag], 
                    [param project id được chọn], 
                    <datetime now>, 
                    <datetime now>
                );
             ```
            - Thêm mới tag of task
                ```sql
                INSERT INTO project_tag (project_tagable_id, project_tagable_type, project_tag_name_id, created_at, updated_at)
                VALUES (
                    [id của task (#sql1)], 
                    'task', 
                    [id của project_tag_name], 
                    <datetime now>, 
                    <datetime now>
                );
             ```
        - Trạng thái xử lý:
            - Thành công => redirect về màn hình danh task của dự án được chọn. 
            - Thất bại => hiển thị thông báo. Ngưng xử lý