
# User - Task - Detail Task

- **Quyền truy cập:** Mọi user thuộc project
- Xem detail thông tin task

## Di chuyển màn hình

```mermaid
graph LR
A[Task list] --Click tạo detail--> B[Detail task]
B --Quay lại--> A
```

## Logic
- ### Thông tin task
    - Câu sql lấy thông tin task
         ```sql
        SELECT task.name, task.id, task.description, task.start_time, task.end_time, task.is_draft, task.is_new, task.is_close,task.percent_process, task.priority, project_task_status.name as status_name, task_type.name as type_name, user_profile.name as assign_name, project.name as project_name, create_by.name as user_create_by_name, task.created_at
        FROM task
        LEFT JOIN project_task_status
            ON project_task_status.project_task_status_id = project_task_status.id
        LEFT JOIN task_type
            ON project_task_status.task_type_id = task_type.id
        LEFT JOIN task_assign_user
            ON task_assign_user.task_id = task.id
        LEFT JOIN user_profile
            ON user_profile.user_id = task_assign_user.user_id
        LEFT JOIN project
            ON project.id = task.project_id
         LEFT JOIN user_profile as create_by
            ON user_profile.user_id = task.created_by
        where task.id = [id task]
            and task.deleted_at = null
        ```
    - Câu sql lấy thông tin tag của task
         ```sql
        SELECT project_tag_name.name
        FROM project_tag
        LEFT JOIN project_tag_name
            ON project_tag_name.id = project_tag.id
        
        where project_tag.project_tagable_type = 'task'
            and project_tag.project_tagable_id = [id task]
        ```
    - Hiển thị thông tin task
        - Tên dự án: BIC (project_name)
        - Tên task: Sửa text left menu, breadcrumb (task.name)
        - Status task: New (status_name)
        - Loại task: Feature (type_name)
        - Người nhận: Kim Nguyễn (assign_name)
        - Ngày bắt đầu: 2023-03-10 (task.start_time)
        - Ngày kết thúc: 2023-03-15 (task.end_time)
        - Người tạo: Thạch Anh Tuấn (user_create_by_name)
        - Ngày tạo: Thạch Anh Tuấn (task.created_at)
        - Nội dung: ...... (task.description)
        - Tags: abc, dce, .... (array project_tag_name.name)

- ### Hiển thị task group
    - Hiển thị danh sách group của task: 
        - Tên group: [ABC](link_group_detail)
        - Câu sql
        ```sql
            SELECT task_group.name, task_group.id
            FROM task_group
            LEFT JOIN user_task_group
                ON task_group.id = user_task_group.task_group_id
            where task_group.project_id = [id project of task]
                and user_task_group.task_id = [id task]
                and task_group.deleted_at = null
                and user_task_group.deleted_at = null
        ```
    - Nút chỉnh sửa group => Luồng như bên màn hình chỉnh sửa

- ### Thông tin comment
    - Câu SQL lấy thông tin comment
        ```sql
            SELECT task_change_log_detail.id, task_change_log_detail.type, task_change_log_detail.state_new, task_change_log_detail.state_old, user_profile. name, task_change_log_detail.created_at
            FROM task_change_log
            LEFT JOIN task_change_log_detail
                ON task_change_log_detail.task_change_log_id = task_change_log.id
            LEFT JOIN user_profile
                ON user_profile.user_id = task_change_log.created_by
            WHERE task_change_log.task_id = <task_ID>
        ```

    - Các trường cần hiển thị
        - Tên User đã viết commnet: 
        - Thời gian đã viết commnet:
        - Log: Name (Nếu có)
        - Log: Status (Nếu có)
        - Log: Assignee (Nếu có)
        - Log: StarSDate (Nếu có)
        - Log: EndDate (Nếu có)
        - Log: Priority (Nếu có)
        - Log: % Done (Nếu có)
        - Log: Description (Nếu có)
        - Log: File (Nếu có)
        - Text comment (Nếu có)

    > #### Kết quả mong muốn
        >
        > Updated by [Hồng Hoa](http://link_page_detail_user) [about 1 month](http://link_hien_thi_change_1month) ago
        > - **Name** changed 'ABC' to 'DEF'
        > - **Status** changed from New to Resolved
        > - **Assignee** changed from Hồng Hoa to Thiện Đoàn
        > - **StartDate** changed '2023-03-10' to '2023-03-09'
        > - **EndDate** changed '2023-03-10' to '2023-03-12'
        > - **Priority** changed from low to medium
        > - **% Done** changed from 70 to 90
        > - **Description** updated [(diff)](http://link_page_detail)
        > - **File** ~~test12345.png~~ deleted
        > - **File** [test1.xlsx](http://link_download) added
        > - [Text comment]
       >