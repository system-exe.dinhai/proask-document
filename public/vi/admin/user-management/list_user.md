
# Admin - User Management - List User

## Search user

### Mô tả: 
- Thực hiện search danh sách user theo những mục tiêu mong muốn nhất định

### Logic: 
- Search user theo key input

|Item|Type|Search|Validate|Table|Column|Note|
|----|----|----|----|----|----|----|
|NAME|Input|Like|(Note)|user_profiles|name|(Note)|
|EMAIL|Input|Like|(Note)|users|email|(Note)|
|ADMIN|Checkbox|Bằng|(Note)|users|is_admin|hiển thị 2 check user_profilesbox: Admin(1) và User(0)|
|UNAPPROVED|Checkbox|Bằng|(Note)|users|is_approved|hiển thị danh sách user đã được APPROVED(1) hoặc chưa được APPROVED(0)|
|BANNED|Checkbox|Bằng|(Note)|users|is_banned|hiển thị danh sách user đã bị BANNED(1) hoặc không bị BANNED(0)|
|CREATED|Date time|Like|Ngày From nhỏ hơn ngày To|users|created_at|hiển thị 2 input From - To|

- Kết quả search sắp xếp giảm dần theo ngày tạo mới
- Trường hợp search không có data:
	- Hiển thị text thông báo: Không tồn tại user
	```
	SELECT *
	FROM users, user_profiles
	WHERE users.id = user_profiles.id
	ORDER BY ASC users.created_at
	```
- Danh sách user chưa được APPROVED
	```
	SELECT *
	FROM users, user_profiles
	WHERE users.id = user_profiles.id AND users.is_approved = 0
	ORDER BY ASC users.created_at
	```
- Danh sách user bị BANNED
	```
	SELECT *
	FROM users, user_profiles
	WHERE users.id = user_profiles.id AND users.is_banned = 1
	ORDER BY ASC users.created_at
	```
- Không search được user đã được delete khỏi hệ thống (user có `'user.[deleted_at] != NULL'` )
	```
	SELECT *
	FROM users, user_profiles 
	WHERE users.id = user_profiles.id AND users.deleted_at != NULL
	ORDER BY ASC users.created_at
	```
- Trường hợp search Ngày CREATED To nhỏ hơn From:
	- Hiển thị text validate: Ngày Form phải nhỏ hơn To
- Phân trang: 10 user per page 
	- Hiển thị selectbox phân trang gồm các item: 10, 50, 100
- Hiển thị tổng số lượng record
- Hiển thị button:
	- Search 
		- Thực hiện search
	- Clear  
		- Xóa những giá trị đã được nhập

## Hiển thị list user

### Mô tả: 
- Hiển thị danh sách user

### Logic:
- Hiển thị danh sách user giảm dần theo ngày tạo mới  
- Trường hợp không có data:
	- Hiển thị text thông báo: Không tồn tại user
	```
	SELECT *
	FROM users, user_profiles
	WHERE users.id = user_profiles.id 
	ORDER BY ASC users.created_at 
	```
- Nếu user đã được delete:  
	 - delete khỏi hệ thống:  không hiển thị (user có `'user.[deleted_at] != NULL'` )
		```
		SELECT *
		FROM users, user_profiles 
		WHERE users.id = user_profiles.id AND users.deleted_at != NULL
		ORDER BY ASC users.created_at
		```
- Nếu user đã bị banned:  hiển thị record dưới dạng mờ 
	```
	SELECT *
	FROM users, user_profiles 
	WHERE users.id = user_profiles.id AND users.is_banned == 1
	ORDER BY ASC users.created_at
	```
- Phân trang: 10 user per page 
	- Hiển thị selectbox phân trang gồm các item: 10, 50, 100
- Hiển thị tổng số lượng record
```
SELECT *
FROM users, user_profiles
WHERE users.id = user_profiles.id
ORDER BY ASC users.created_at
COUNT users.id
```
- NO: số thứ tự
- NAME: Tên user 
```
SELECT users.name
FROM users, user_profiles
WHERE users.id = user_profiles.id
ORDER BY ASC users.created_at
```
- EMAIL: hiển thị danh sách email không bị trùng
- CREATED: hiển thị ngày tạo user
	- Định dạng: DD/MM/YYYY HH:MM
- UNAPPROVED: hiển thị Checkbox
	- Có thể thao tác checked(1) trên page list này
	- Nếu chưa được approved: UNCHECK(0)
		- ENABLE Checkbox
		```
		SELECT *
		FROM users, user_profiles
		WHERE users.id = user_profiles.id AND users.is_approved = 0
		ORDER BY ASC users.created_at
		```
	- Nếu đã được approved: CHECKED (1)
		- DISABLE Checkbox
		```
		SELECT *
		FROM users, user_profiles
		WHERE users.id = user_profiles.id AND users.is_approved = 1
		ORDER BY ASC users.created_at
		```
- ICON PENCIL: hiển thị icon
	- Link đến page detail user  (theo ID)
	- Di chuyển link cùng tab trình duyệt
- ICON EYE: hiển thị icon
	- link_page_view_thông_tin  (theo ID)
	- Di chuyển link cùng tab trình duyệt
- ICON ADMINISTRATOR: hiển thị icon
	- Trường hợp là admin: hiển thị icon [ICON ADMINISTRATOR]
	- Trường hợp không là admin: không hiển thị icon [ICON ADMINISTRATOR]
- Sort
  
|Item|Type|Sort|
|----|----|----|
|NAME|Text|Tăng dần/Giảm dần(UTF8)|
|EMAIL|Text|Tăng dần/Giảm dần(UTF8)|
|ADMIN|True/False|1/0|
|UNAPPROVED|True/False|1/0|
|BANNED|True/False|1/0|
|CREATED|Date time|Tăng dần/Giảm dần(Date time)|

	```
	SELECT *
	FROM users, user_profiles 
	WHERE users.id = user_profiles.id AND users.deleted_at != NULL
	ORDER BY ASC users.created_at
	```
- CHECKALL: cho lựa chọn 1, 1 số hoặc tất cả record
- Hiển thị button:
	- Create user
		- Đi đến page tạo mới user
	- Approved All
		- Trường hợp không chọn record nào: click button Approved All
			- Thông báo thành công 
			- Không thao tác DB
		- Trường hợp chọn 1, 1 số hoặc tất cả record record nào: click button Approved All
			- Thông báo thành công 
			- Trường hợp user đã có is_approved = 1 thì bỏ qua record đó
			- Thao tác cập nhật DB
			```
			UPDATE users
			SET is_approved = 1
			WHERE id = id user đã lựa chọn AND is_approved = 0
			```
	- Export CSV
		- Trường hợp không chọn record nào: click button Export CSV
			- Thông báo thành công 
			- Không thao tác DB
		- Trường hợp chọn 1, 1 số hoặc tất cả record record nào: click button Export CSV
			- Thông báo thành công 
			- Export file CSV với record đã chọn
				- Tên file: users_YYYYMMDD.csv
				- Nội dung file CSV: hiển thị như trang list

## Kết quả mong muốn

|CHECKALL|NO|NAME|EMAIL | CREATED|UNAPPROVED||
|----|----|----|----|----------|----|----|
|CHECKBOX|1|TEST A `'[ICON ADMINISTRATOR]'` |test1@gmail.com|02/01/2023 06:54||[ICON PENCIL](http://link_page_edit_thông_tin_user) <br>[ICON EYE](http://link_page_view_thông_tin) |
|CHECKBOX|2|TEST A|test2@gmail.com|02/01/2023 06:54|UNCHECK|[ICON PENCIL](http://link_page_edit_thông_tin_user) <br>[ICON EYE](http://link_page_view_thông_tin) |
|CHECKBOX|3|TEST A |test3@gmail.com|02/01/2023 06:54|CHECKED|[ICON PENCIL](http://link_page_edit_thông_tin_user) <br>[ICON EYE](http://link_page_view_thông_tin) |
|CHECKBOX|4|TEST A `'[ICON ADMINISTRATOR]'` |test4@gmail.com|02/01/2023 06:54||[ICON PENCIL](http://link_page_edit_thông_tin_user) <br>[ICON EYE](http://link_page_view_thông_tin) |
|CHECKBOX|5|TEST A |test5@gmail.com|02/01/2023 06:54||[ICON PENCIL](http://link_page_edit_thông_tin_user)<br>[ICON EYE](http://link_page_view_thông_tin) |
|CHECKBOX|6|TEST A `'[ICON ADMINISTRATOR]'`  |test6@gmail.com|02/01/2023 06:54||[ICON PENCIL](http://link_page_edit_thông_tin_user) <br>[ICON EYE](http://link_page_view_thông_tin)|
|CHECKBOX|7|TEST A |test7@gmail.com|01/14/2023 06:54||[ICON PENCIL](http://link_page_edit_thông_tin_user) <br>[ICON EYE](http://link_page_view_thông_tin)|
|CHECKBOX|8|TEST A|test8@gmail.com|02/01/2023 06:54||[ICON PENCIL](http://link_page_edit_thông_tin_user) <br>[ICON EYE](http://link_page_view_thông_tin) |
|CHECKBOX|9|TEST A|test9@gmail.com|02/01/2023 06:54||[ICON PENCIL](http://link_page_edit_thông_tin_user) <br>[ICON EYE](http://link_page_view_thông_tin) |
|CHECKBOX|10|TEST A|test10@gmail.com|02/01/2023 06:54||[ICON PENCIL](http://link_page_edit_thông_tin_user) <br>[ICON EYE](http://link_page_view_thông_tin) |
|CHECKBOX|----|`'user_profiles.[name]  where user_profiles.[ID] =  user.[ID]'`<br> `'IF(user.[is_admin]==TRUE show ICON ADMINISTRATOR) ELSE NULL'`|`'user.[email]'`|`'user.[created_at]'`|`'user.[is_approved]'`||





