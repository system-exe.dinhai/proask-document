# Admin - User Management - SubDashboard User
- **Quyền truy cập:** User có quyền Admin
- Hiển thị danh sách user với các thông số liên quan đến dự án và task liên quan.
- Xem tổng quan về dự án và task của từng user.

## Di chuyển màn hình

```mermaid
graph LR
A[Menu top] --Click vào menu--> B[Quản lý user]
B --Click vào user--> C[Chi tiết Detail user]
C --Quay lại--> B
C--Xem project--> D[Chi tiết project]
C--Xem chi tiết user--> E[Chi tiết Quản lý User]

```


## Chi tiết detail user
- Từ màn hình [Quản lý User] click vào [Chi tiết Detail user] trên từng user ->màn hình [Chi tiết Detail user].
- Button quay về trang trước: quay về trang list user
- Thực hiện câu sql để lấy những thông tin cần thiết:
```sql
SELECT  user.id, user.email, user_profile.name, count(project_user.project_id) as total_project,
count(task_assign_user.task_id) as total_task,
count(select task_id from task where task.end_time < [Giá trị ngày hiện tại]) as total_task_deadline,
count(select task_id from task where task.status = 'closed') as total_task_success,
FROM User
LEFT JOIN user_profile
ON  user.id  = user_profile.user_id
LEFT JOIN project_user
ON  user.id  = project_user.user_id
LEFT JOIN task_assign_user
ON  user.id  = task_assign_user.user_id
LEFT JOIN task
ON  task_assign_user.task_id = task.id
where user.id = [param id được chọn]
```

    - Thông tin User:
        - Họ tên: Kim Nguyễn  - `user.name`
        - Email: kim@gmail.com - `user_profile.email`
        - Phone: 0123456789 - `user_profile.phone`
        - Role: developer- `user_profile.role`
        - [Xem chi tiết](http://link_chi_tiết_thông_tin_user)
    - Phần trăm hoàn thành task: Tổng task có status là success / Tổng số task .
    - project: Tổng số dự án đang tham gia.	
    - task: Tổng số task đang tham gia.
    - task bị trễ: Tổng số task tham gia bị trễ deadline. Hiển thị nổi bật để dễ nhìn thấy.

- Các task mới nhât: hiển thị 3 task mới nhất

    ```sql
    SELECT task.name, task.priority, task.status
    FROM User
    LEFT JOIN task_assign_user
    ON  user.id  =  task_assign_user.user_id
    LEFT JOIN task
    ON  task_assign_user.task_id  =  task.id
    where user.id = [param id được chọn]
    where task.is_draft = false
    where task.deleted_at = null
    where task.status = 'open'
    order by created_at desc
    limit 3
    ```

    > Hiển thị danh sách

    | STT |Tên task |priority
    |------|-----|-----|
    | 1 |[Fix bug màn hình user](http://link_deatil_task) |medium|
    | 2 |[Fix bug login](http://link_deatil_project) |high|
    | |`task.name`|`task.status`|

- Các dự án đang tham gia:

    ```sql
    SELECT  user.id, user.email, user_profile.name, count(project_user.project_id) as total_project,
    count(task_assign_user.task_id) as total_task,
    count(select task_id from task where task.end_time < [Giá trị ngày hiện tại]) as total_task_deadline,
    FROM User
    LEFT JOIN user_profile
    ON  user.id  =  user_profile.user_id
    LEFT JOIN project_user
    ON  user.id  =  project_user.user_id
    LEFT JOIN task_assign_user
    ON  user.id  =  task_assign_user.user_id
    LEFT JOIN task
    ON  task_assign_user.task_id = task.id
    where user.id = [param id được chọn]
    ```
    > Hiển thị danh sách

    | STT |Tên project |Vai trò|Trạng thái| Tổng task|task trễ|Người tạo|
    |------|-----|-----|-----|-----|-----|-----|
    | 1 |[DOCON](http://link_deatil_project) |developer|in_progress|12|2|Thạch Anh Tuấn|
    | 2 |[BIC2](http://link_deatil_project) |developer|completed|5|0|Thạch Anh Tuấn|
    | |`project.name`|`project_user.role`|`project.status`|`total_task`|`total_task_deadline`|`project.created_by`|

    - `total_task`: Tính tổng task của từng dự án.
    - `total_task_deadline`: Tính tổng task đang bị trễ deadline của từng dự án.


## Chart

- Tiến trình các dự án tham gia:
    - Chọn năm để xem: giá trị mặc định lúc load lên là năm hiện tại
    - Hiển thị biểu đồ xem được danh sách các project 

        ```mermaid
        gantt
            title Tiến trình của các dự án
            dateFormat  YYYY-MM-DD
            section Section
            DOCON           :a1, 20222-01-01,1y
            BIC2     :a2, 20222-06-01,1y
        ```

        
- Biểu đồ trạng thái của các task theo tháng
    - Chọn năm, tháng (mặc định tháng hiện tại)
    - Chọn dự án (mặc định tất cả các dự án)
    - Biểu đồ cột chồng status task
  