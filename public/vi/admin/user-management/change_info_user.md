# Admin - User Management - Change Info User

## Di chuyển màn hình

```mermaid
graph LR
L[Danh sách người dùng]
P{Popup: Xác nhận } --'Yes'--> L
L -- 'A: Chỉnh sửa'--> B[Thông tin người dùng]--'Lưu'-->P
B  --'Quay lại: ko có thông tin đang edit' --> L
B --'Quay lại: có data đang edit'-->P
B --'Xóa'-->P
B --'Khôi phục'-->P
```

## Button

|Tên Button  |Điều Kiện Hiện  | Điều Kiện Ẩn |
|--|--|--|
|  Lưu| user.deleted_at = null |user.deleted_at != null
|  Xóa| user.deleted_at = null |user.deleted_at != null
|  Khôi phục|user.deleted_at != null |user.deleted_at = null
|  Quay lại| Luôn hiện


## Chỉnh sửa


- Load data từ DB lên form 
> **Luu ý** :  khi load data lên form check nếu **user.is_active = false** thì disable toàn bộ input ngược lại hiện lên như bảng dưới

```sql
    SELECT  *  
    FROM  user  
    LEFT  JOIN  userProfile 
    ON  user.id = userProfile.user_id ;
```

- Chỉnh sửa data ở các  mục input như bảng sau đó bấm [Lưu] --> **Lưu data** xuống DB tương ứng kèm **Show Message** "Đã chỉnh sửa thông tin người dùng <Tên người dùng>"



|**Input fill** |**Input type**   | **Enable/Disable** | **Required**|**DB** |**Validate**|
|----|------------|---|---|-------|------------|
|Mã nhân viên  | Text box |Disable| O |  user.user_id| Text (25) / Unique |
|email  | Text box |Disable| O |  user.email| Text (255) / mail|
|Tên nhân viên | Text box |Enable| O | userProfile.full_name| Text (255) / không chứa kí tự đặc biệt, số|
|Thông tin sơ lược | Text box |Enable| X | userProfile.resume| Text (552)|
|Chức vụ  | Drop box |Enable| O |  userProfile.role|  |
|Số điện thoại  | Text box |Enable| X |  userProfile.phone| Int (10) |
|Ngày sinh  | Text box |Enable| X|  userProfile.birthday| date|
|Địa chỉ | Text box | Enable|X |  userProfile.addess| Text (255)|
|Ảnh đại diện | Image|Enable| X |  userProfile.has_avatar| href|
| |  | |   |  userProfile.updated_at = date time update|


```SQL
        UPDATE userProfiles
        SET full_name = 'Alfred Schmidt', resume= 'Frankfurt', role = 'Dev', phone = '0388677777', birthday '07/01/1999', addess = '345 Cộng hòa', has_avatar = 'href', updated_at 
        WHERE userProfiles.user_id = 1;
```   

> **Ảnh đại diện** : 
- khi has_avatar = 1 show ảnh đã đưuọc chọn
- khi has_avatar = 0 show chữ đại diện VD=> Hồng -> show chữ H

> **Nút [Quay lại]** :  khi có thông tin đang chỉnh sửa bấm nút[Quay lại] --> show popup confirm 
- Yes--> quay về màn hình [Danh sách người dùng]
- No --> ở lại MH [Thông tin người dùng] close popup

 

## Xóa
- Button chỉ hiện khi ```user.is_active  = true```
- Từ màn hình [Thông tin] click button [Xóa] --> Show **popup xác nhận**
    - **Yes**: update ```user.is_active = false``` ```user.update_at =  date time xóa``` --> quay lại màn hình [Danh sách người dùng]
    - **No**: Đóng popup

## Khôi phục
- Button chỉ hiện khi ```user.is_active  = false```
- Click button **[Khôi phục]**  --> hiện dialog **[Xác nhận khôi phục]**
    - **Yes**: update lại  ```user.is_active = true``` ``` user.updated_at = Date time khôi phục``` --> quay về MH **[Danh sách người dùng]** show Message **"Đã khôi phục người dùng <Tên người dùng>"**
    - **No**: Đóng dialog
 

## Quay lại   
- Khi có thông tin đang chỉnh sửa bấm nút[Quay lại] --> show popup confirm 
    - **Yes**--> quay về màn hình [Danh sách người dùng]
    - **No** --> ở lại MH [Thông tin người dùng]
- Khi không có thông tin đang chỉnh sửa bấm nút [Quay lại] --> về màn hình list


    |**Input fill** |**Input type**   | **Enable/Disable** | **Required**|**DB** |**Validate**|
    |----|------------|---|---|-------|------------|
    |Mã nhân viên  | Text box |Disable| O |  user.user_id| Text (25) / Unique |
    |email  | Text box |Disable| O |  user.email| Text (255) / mail|
    |Tên nhân viên | Text box |Enable| O | userProfile.full_name| Text (255) / không chứa kí tự đặc biệt, số|
    |Thông tin sơ lược | Text box |Enable| X | userProfile.resume| Text (552)|
    |Chức vụ  | Drop box |Enable| O |  userProfile.role|  |
    |Số điện thoại  | Text box |Enable| X |  userProfile.phone| Int (10) |
    |Ngày sinh  | Text box |Enable| X|  userProfile.birthday| date|
    |Địa chỉ | Text box | Enable|X |  userProfile.addess| Text (255)|
    |Ảnh đại diện | Image|Enable| X |  userProfile.has_avatar| href|
    | |  | |   |  userProfile.updated_at = date time update|