# Admin - User Management - Add new user

## Di chuyển màn hình

```mermaid
graph LR
L[Danh sách người dùng] -- 'Thêm mới' --> A[Thêm mới người dùng ]
 A[Thêm mới người dùng ]  --'Quay lại: có data đang edit'-->P{Popup: Xác nhận } --'Yes'--> L
A --'Quay lại: ko có thông tin đang edit' --> L
A--'Đăng kí' --> P
```

## Button


|Tên Button  |Điều Kiện Hiện  |
|--|--|
|  Đăng kí| Luôn hiện |
|  Quay Lại| Luôn hiện |

## Đăng kí
- Nhập data vào các mục input như bảng dưới sau đó bấm [Đăng kí]  thì thực thi như dưới đây:
    - **Lưu data** xuống DB tương ứng kèm **Show Message** "Đã đăng kí thành công người dùng <Tên người dùng>"

        |**Input fill** |**Input type**   | **Enable/Disable** | **Required**|**DB** |**Validate**|
        |----|------------|---|---|-------|------------|
        |Mã nhân viên  | Text box |Enable| O |  user.user_id| Text (25) / Unique |
        |email  | Text box |Enable| O |  user.email| Text (255) / mail/unique|
        |Tên nhân viên | Text box |Enable| O | userProfile.full_name| Text (255) / không chứa kí tự đặc biệt, số|
        |Thông tin sơ lược | Text box |Enable| X | userProfile.resume| Text (552)|
        |Chức vụ  | Drop box |Enable| O |  userProfile.role|  |
        |Số điện thoại  | Text box |Enable| X |  userProfile.phone| Int (10) |
        |Ngày sinh  | Text box |Enable| X|  userProfile.birthday| date|
        |Địa chỉ | Text box | Enable|X |  userProfile.addess| Text (255)|
        |Ảnh đại diện | Image|Enable| X |  userProfile.has_avatar| href|
        | |  |  | |  user.is_active = true| |
        | |  |  | |  userProfile.user_id = user.id|

        
        ```SQL
        INSERT INTO users (user_id, email, is_active)
        VALUES ('SVN712', 'nguyenvanhau@gmail.com', 'true');
        
        INSERT INTO userProfiles (full_name, resume, role, phone, birthday, addess, has_avatar, user_id)
        VALUES ('Nguyen Văn a', 'Giới thiệu', 'Dev', '03886xxxx9', '35 cộng hòa', '1', 'SVN712');
        ```

        **Lưu ý:** 
        - Khi **Đăng kí** check nếu mail đã tồn tại và ```user.is_active = false ``` thì show message thông báo cho user liên hệ vs admin để active tài khoản    
        - Khi **Đăng kí** check nếu mail đã tồn tại trong **Register**   thì show message thông báo đang đợi duyệt
    - **Gửi mail** chứa passwork tới người dùng




## Quay lại   
- Khi có thông tin đang chỉnh sửa bấm nút[Quay lại] --> show popup confirm 
    - **Yes**--> quay về màn hình [Danh sách người dùng]
    - **No** --> ở lại MH [Thêm mới người dùng]
- Khi không có thông tin đang chỉnh sửa bấm nút [Quay lại] --> về màn hình list