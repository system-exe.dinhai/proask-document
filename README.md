# ProAsk Document

## Cấu Trúc Thư Mục
```
	|public
		|vi
			|Feature Folder
				|Document Feature Markdown
```
##  To do List
- [ ] Quản lý dự án: Tạo, xóa, sửa, đặt tên và mô tả dự án.
- [ ] Quản lý người dùng: Đăng ký, đăng nhập, quản lý thông tin người dùng, phân quyền người dùng.
- [ ] Quản lý công việc: Tạo, xóa, sửa, phân loại và gán công việc cho người dùng.
- [ ] Quản lý thời gian: Theo dõi thời gian làm việc của người dùng trên từng công việc và dự án.
- [ ] Quản lý tài liệu: Tải lên, tải xuống và chia sẻ tài liệu liên quan đến dự án.
- [ ] Quản lý vấn đề: Đưa ra, theo dõi và giải quyết các vấn đề liên quan đến dự án.
- [ ] Quản lý phiên bản: Quản lý các phiên bản sản phẩm và theo dõi tiến độ sản phẩm.
- [ ] Báo cáo và thống kê: Tạo báo cáo về tiến độ dự án, thời gian hoàn thành và số lượng công việc được hoàn thành.
- [ ] 
## Role
|Name|Role|
|-|-|
|Đình Ái|Design App, Design UI, API, UI, Testing|
|Thanh Tân|Design App, Design UI, UI, Testing|
|Thùy Kim|Design App, API, Testing|
|Hồng Hoa|Design App, Design UI, Testing|
|Phú Hồng|Design App, Design UI, Testing|
|Ngọc Thiện|Design App, API, UI, Testing|
|Huỳnh Đức|Design UI, UI|